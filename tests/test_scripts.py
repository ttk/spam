import os
import subprocess
import unittest

import h5py
import numpy
import scipy
import spam.datasets
import spam.deformation
import spam.DIC
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh
import tifffile

# This for hiding script output
FNULL = open(os.devnull, "w")

# This for showing it
# FNULL = None

# We also check 2D slice so don't displace in Z or rotate around an axis other than Z
refTranslation = numpy.array([0.0, 15, 0.0])
refRotation = numpy.array([3.0, 0.0, 0.0])


class TestAll(spam.helpers.TestSpam):

    # def __init__(self, *args, **kwargs):
    #     super(testAll, self).__init__(*args, **kwargs)
    #     coverage.process_startup()

    def test_gridDICtools(self):

        #######################################################
        # Step 1 generate files
        #######################################################
        # Run on snow data, with bin2 registration, without output
        # load 3D image
        snowRef = spam.datasets.loadSnow()
        snowRef2D = snowRef[50]
        # save it locally
        tifffile.imwrite("snow-ref.tif", snowRef)
        os.chmod("snow-ref.tif", 0o666)
        tifffile.imwrite("snow-ref-2D.tif", snowRef2D)
        os.chmod("snow-ref-2D.tif", 0o666)

        # Generate deformed image
        Phi = spam.deformation.computePhi({"t": refTranslation, "r": refRotation})
        snowDef = spam.DIC.applyPhi(snowRef, Phi=Phi)
        # save it locally
        tifffile.imwrite("snow-def.tif", snowDef)
        os.chmod("snow-def.tif", 0o666)
        # Plus 2D version
        # tifffile.imwrite("snow-def-2D.tif", spam.DIC.applyPhiPython(snowRef2D[numpy.newaxis, ...], Phi=Phi))
        tifffile.imwrite("snow-def-2D.tif", snowDef[50])
        os.chmod("snow-def-2D.tif", 0o666)

        snowDefOnlyDisp = spam.DIC.applyPhi(snowRef, Phi=spam.deformation.computePhi({"t": refTranslation}))
        # save it locally
        tifffile.imwrite("snow-def-onlyDisp.tif", snowDefOnlyDisp)
        os.chmod("snow-def.tif", 0o666)

        # Mask for pixel search
        snowMask = numpy.ones_like(snowDef, dtype="<u1")
        snowMask[0] = 0
        snowMask[-1] = 0
        snowMask2D = numpy.ones_like(snowRef2D, dtype="<u1")
        snowMask2D[0] = 0
        snowMask2D[-1] = 0
        tifffile.imwrite("snow-mask-ref.tif", snowMask)
        os.chmod("snow-mask-ref.tif", 0o666)
        tifffile.imwrite("snow-mask-ref-2D.tif", snowMask2D)
        os.chmod("snow-mask-ref-2D.tif", 0o666)
        tifffile.imwrite("snow-mask-def.tif", (snowDef > 0).astype("u1"))
        os.chmod("snow-mask-def.tif", 0o666)
        # tifffile.imwrite("snow-mask-2D.tif", snowMask[50])
        # os.chmod("snow-mask-2D.tif", 0o666)

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"t": 0.8 * refTranslation, "r": 0.8 * refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg-onlyRot.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"r": refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close
        spam.helpers.writeRegistrationTSV(
            "snow-grid-ereg-onlyDisp.tsv",
            (numpy.array(snowRef.shape) - 1) / 2.0,
            {
                "Phi": spam.deformation.computePhi({"t": refTranslation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close in 2D
        spam.helpers.writeRegistrationTSV(
            "snow-grid-2D-ereg.tsv",
            [0, snowRef.shape[1] - 1 / 2, snowRef.shape[2] - 1 / 2],
            {
                "Phi": spam.deformation.computePhi({"r": 0.8 * refRotation, "t": 0.8 * refTranslation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        # generate a fake "eye reg" initial guess which is close in 2D
        spam.helpers.writeRegistrationTSV(
            "snow-grid-2D-ereg-onlyRot.tsv",
            [0, snowRef.shape[1] - 1 / 2, snowRef.shape[2] - 1 / 2],
            {
                "Phi": spam.deformation.computePhi({"r": refRotation}),
                "error": 0,
                "iterations": 0,
                "returnStatus": 2,
                "deltaPhiNorm": 1,
            },
        )

        #######################################################
        # Step 2 check spam-reg functionality
        #######################################################
        # Just run a simple registration, with a guess from "ereg"
        exitCode = subprocess.call(
            [
                "spam-reg",
                "-bb",
                "2",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid",
                "-mar",
                "20",
                "-pf",
                "snow-grid-ereg.tsv",
                "-mf1",
                "snow-mask-ref.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        regResult = spam.helpers.readCorrelationTSV("snow-grid-registration.tsv")
        transformation = spam.deformation.decomposePhi(regResult["PhiField"][0])
        # print(transformation['t'])
        # print(transformation['r'])
        self.assertTrue(numpy.allclose(refTranslation, transformation["t"], atol=0.01))
        self.assertTrue(numpy.allclose(refRotation, transformation["r"], atol=0.01))

        # Just run a simple registration in 2D!
        exitCode = subprocess.call(
            [
                "spam-reg",
                "-bb",
                "2",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-2D",
                "-mar",
                "20",
                "-pf",
                "snow-grid-2D-ereg.tsv",
                "-mf1",
                "snow-mask-ref-2D.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        reg2dResult = spam.helpers.readCorrelationTSV("snow-grid-2D-registration.tsv")
        transformation2d = spam.deformation.decomposePhi(reg2dResult["PhiField"][0])
        # print(transformation['t'])
        # print(transformation['r'])
        self.assertTrue(numpy.allclose(refTranslation, transformation2d["t"], atol=0.01))
        self.assertTrue(numpy.allclose(refRotation, transformation2d["r"], atol=0.01))

        #######################################################
        # Step 3 check spam-pixelSearch functionality
        #######################################################
        # Step 3a load initial guess and just search around it...
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-registration.tsv",
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3a",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3a = spam.helpers.readCorrelationTSV("snow-grid-3a-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3a["pixelSearchCC"][PSresult3a["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3a["PhiField"][PSresult3a["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3b load initial ONLY ROTATION guess and do a big search around the applied displacement
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-ereg-onlyRot.tsv",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3b",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-mf2",
                "snow-mask-def.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3b = spam.helpers.readCorrelationTSV("snow-grid-3b-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3b["pixelSearchCC"][PSresult3b["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3b["PhiField"][PSresult3b["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3c load "only rotation" result from above and just do a +-1 search around it
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-3b-pixelSearch.tsv",
                "-sr",
                # "-3", "3", "-3", "3", "-3", "3",
                "-1",
                "1",
                "-1",
                "1",
                "-1",
                "1",
                # "0", "0", "0", "0", "0", "0",
                "-glt",
                "5000",
                "-hws",
                "10",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3c",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-mf2",
                "snow-mask-def.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3c = spam.helpers.readCorrelationTSV("snow-grid-3c-pixelSearch.tsv", readPixelSearchCC=True)
        print(numpy.median(PSresult3c["pixelSearchCC"][PSresult3c["returnStatus"] == 1]))
        self.assertTrue(0.95 < numpy.median(PSresult3c["pixelSearchCC"][PSresult3c["returnStatus"] == 1]))
        # print(
        #     PSresult3b['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1,
        #     PSresult3c['returnStatus']==1),0:3,-1]-PSresult3c['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1,
        #     PSresult3c['returnStatus']==1),0:3,-1]
        # )
        self.assertTrue(
            0
            == numpy.sum(
                PSresult3b["PhiField"][numpy.logical_and(PSresult3b["returnStatus"] == 1, PSresult3c["returnStatus"] == 1)]
                - PSresult3c["PhiField"][numpy.logical_and(PSresult3b["returnStatus"] == 1, PSresult3c["returnStatus"] == 1)]
            )
        )

        # Step "3d"!!! check 2D mode
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-2D-ereg-onlyRot.tsv",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "5",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-tif",
                "-pre",
                "snow-grid-2d3d",
                "-mf1",
                "snow-mask-ref-2D.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3d = spam.helpers.readCorrelationTSV("snow-grid-2d3d-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.90 < numpy.nanmedian(PSresult3d["pixelSearchCC"][PSresult3d["returnStatus"] == 1]))
        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3d["PhiField"][PSresult3d["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )

        # Step 3e NO ROTATION IMAGE big search around the applied displacement, this allows us to check the measured values of displacement
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-sr",
                "{}".format(int(refTranslation[0] - 2)),
                "{}".format(int(refTranslation[0] + 2)),
                "{}".format(int(refTranslation[1] - 2)),
                "{}".format(int(refTranslation[1] + 2)),
                "{}".format(int(refTranslation[2] - 2)),
                "{}".format(int(refTranslation[2] + 2)),
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3e",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3e = spam.helpers.readCorrelationTSV("snow-grid-3e-pixelSearch.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult3e["pixelSearchCC"][PSresult3e["returnStatus"] == 1]))
        # Check correct values measured
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(PSresult3e["PhiField"][PSresult3e["returnStatus"] == 1, 2, -1]),
                atol=0.1,
            )
        )

        # 3f - use spam-passPhiField
        # Now use spam-passPhiField to apply registration to node-spaced-grid and use it to run a tight pixel-search
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-registration.tsv",
                "-ns",
                "20",
                "-im1",
                "snow-ref.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3f",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3f = spam.helpers.readCorrelationTSV("snow-grid-3f-passed-ns20.tsv")
        self.assertTrue(numpy.isclose(0, numpy.median(PSresult3f["PhiField"][:, 0, -1]), atol=1.0))

        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "snow-grid-3f-passed-ns20.tsv",
                "-sr",
                "-1",
                "1",
                "-1",
                "1",
                "-1",
                "1",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-3g",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult3g = spam.helpers.readCorrelationTSV("snow-grid-3g-pixelSearch.tsv", readPixelSearchCC=True)
        self.assertTrue(0.95 < numpy.median(PSresult3g["pixelSearchCC"][PSresult3g["returnStatus"] == 1]))
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PSresult3g["PhiField"][PSresult3g["returnStatus"] == 1, 0, -1]),
                atol=1.0,
            )
        )
        # self.assertTrue(0 == numpy.sum(  PSresult3b['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1, PSresult3g['returnStatus']==1)]
        # - PSresult3g['PhiField'][numpy.logical_and(PSresult3b['returnStatus']==1, PSresult3g['returnStatus']==1)]))

        #######################################################
        # Step 4 check spam-pixelSearchPropagate
        #######################################################
        # check only with dip
        exitCode = subprocess.call(
            [
                "spam-pixelSearchPropagate",
                "-sp",
                "{}".format(snowRef.shape[0] // 2),
                "{}".format(snowRef.shape[1] // 2),
                "{}".format(snowRef.shape[2] // 2),
                "{}".format(int(refTranslation[0])),
                "{}".format(int(refTranslation[1])),
                "{}".format(int(refTranslation[2])),
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-4a",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        PSresult4a = spam.helpers.readCorrelationTSV("snow-grid-4a-pixelSearchPropagate.tsv", readPixelSearchCC=True)
        # Assert that the CC is nice and high
        self.assertTrue(0.95 < numpy.median(PSresult4a["pixelSearchCC"][PSresult4a["returnStatus"] == 1]))

        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(PSresult4a["PhiField"][PSresult4a["returnStatus"] == 1, 2, -1]),
                atol=0.1,
            )
        )

        #######################################################
        # Step 5 check spam-ldic loading previous results
        #######################################################

        # Step 5a: load registration
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-registration.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "10",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5a",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-tif",
                "-o",
                "3",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5a = spam.helpers.readCorrelationTSV("snow-grid-5a-ldic.tsv")
        dims = LDICresult5a["fieldDims"]

        # Did more than 80% of points converge?
        returnStatusCropped = LDICresult5a["returnStatus"].reshape(dims)[2:-2, 2:-2, 2:-2]
        self.assertTrue(0.8 < numpy.sum(returnStatusCropped == 2) / returnStatusCropped.size)

        # median Rotation within 0.5 deg?
        PhiFieldCropped = LDICresult5a["PhiField"].reshape(dims[0], dims[1], dims[2], 4, 4)[2:-2, 2:-2, 2:-2, :, :]
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(spam.deformation.decomposePhiField(PhiFieldCropped[returnStatusCropped == 2], components="r")["r"][:, 0]),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(PhiFieldCropped[returnStatusCropped == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step 5b: load pixelSearch results with same grid + ug
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-3a-pixelSearch.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "20",
                "snow-ref.tif",
                "snow-def.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5b",
                "-mf1",
                "snow-mask-ref.tif",
                "-mc",
                "1.0",
                "-ug",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5b = spam.helpers.readCorrelationTSV("snow-grid-5b-ldic.tsv")

        # Did more than 80% of points converge?
        self.assertTrue(0.8 < numpy.sum(LDICresult5b["returnStatus"] == 2) / len(LDICresult5b["returnStatus"]))

        # median Rotation within 0.5 deg?
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(
                    spam.deformation.decomposePhiField(LDICresult5b["PhiField"][LDICresult5b["returnStatus"] > 0], components="r",)[
                        "r"
                    ][:, 0]
                ),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5b["PhiField"][LDICresult5b["returnStatus"] == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step "5c" check 2D mode
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-2D-registration.tsv",
                "-glt",
                "5000",
                "-hws",
                "10",
                "-ns",
                "5",
                "snow-ref-2D.tif",
                "snow-def-2D.tif",
                "-od",
                ".",
                "-tif",
                "-pre",
                "snow-grid-2d",
                "-mf1",
                "snow-mask-ref-2D.tif",
                "-mc",
                "1.0",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5c = spam.helpers.readCorrelationTSV("snow-grid-2d-ldic.tsv")

        # Did more than 50% of points converge?
        self.assertTrue(0.5 < numpy.sum(LDICresult5c["returnStatus"] == 2) / len(LDICresult5c["returnStatus"]))

        # median Rotation within 0.5 deg?
        self.assertTrue(
            numpy.isclose(
                refRotation[0],
                numpy.median(
                    spam.deformation.decomposePhiField(LDICresult5c["PhiField"][LDICresult5c["returnStatus"] > 0], components="r",)[
                        "r"
                    ][:, 0]
                ),
                atol=0.5,
            )
        )

        # And the z-displacement is low
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5c["PhiField"][LDICresult5c["returnStatus"] == 2, 0, -1]),
                atol=1.0,
            )
        )

        # Step 5d: load pixelSearchPropagate results with same grid + ug
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-4a-pixelSearchPropagate.tsv",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5d",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5d = spam.helpers.readCorrelationTSV("snow-grid-5d-ldic.tsv")

        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(LDICresult5d["PhiField"][LDICresult5d["returnStatus"] == 2, 2, -1]),
                atol=0.1,
            )
        )

        # Step 5e: try to filter this field based on return status, and interpolate
        exitCode = subprocess.call(
            [
                "spam-filterPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-srs",
                "-srst",
                "1",
                "-cint",
                "-F",
                "all",
                "-pre",
                "snow-grid-5e",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        LDICresult5e = spam.helpers.readCorrelationTSV("snow-grid-5e-filtered.tsv")
        self.assertTrue(
            numpy.isclose(
                refTranslation[0],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[1],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                refTranslation[2],
                numpy.median(LDICresult5e["PhiField"][LDICresult5e["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # Test 5f: use -regs to subtract registration from ldic.
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5a-ldic.tsv",
                "-regs",
                "snow-grid-registration.tsv",
                "-pre",
                "snow-grid-5f",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5f = spam.helpers.readCorrelationTSV("snow-grid-5f-passed-regs-rigid.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5f["PhiField"][LDICresult5f["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5g Same with the full Phi (no difference here)
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5a-ldic.tsv",
                "-regs",
                "snow-grid-registration.tsv",
                "-regsF",
                "all",
                "-pre",
                "snow-grid-5g",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5g = spam.helpers.readCorrelationTSV("snow-grid-5g-passed-regs-all.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5g["PhiField"][LDICresult5g["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5h Same with only displacements
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-regs",
                "snow-grid-ereg-onlyDisp.tsv",
                "-regsF",
                "no",
                "-pre",
                "snow-grid-5h",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5h = spam.helpers.readCorrelationTSV("snow-grid-5h-passed-regs-disp.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5h["PhiField"][LDICresult5h["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5i 2D check
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-grid-2d-ldic.tsv",
                "-regs",
                "snow-grid-2D-registration.tsv",
                "-regsF",
                "rigid",
                "-pre",
                "snow-grid-5i",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        LDICresult5i = spam.helpers.readCorrelationTSV("snow-grid-5i-passed-regs-rigid.tsv")
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 0, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 1, -1]),
                atol=0.1,
            )
        )
        self.assertTrue(
            numpy.isclose(
                0,
                numpy.median(LDICresult5i["PhiField"][LDICresult5i["returnStatus"] >= 1, 2, -1]),
                atol=0.1,
            )
        )

        # 5j Check -skp

        # Use spam-passPhiField to filter the result based on the RS

        exitCode = subprocess.call(
            [
                "spam-filterPhiField",
                "-pf",
                "snow-grid-5d-ldic.tsv",
                "-srs",
                "-cint",
                "-F",
                "no",
                "-od",
                ".",
                "-pre",
                "snow-grid-5j-Guess",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Run ldic with -skp
        exitCode = subprocess.call(
            [
                "spam-ldic",
                "-pf",
                "snow-grid-5j-Guess-filtered.tsv",
                "-glt",
                "5000",
                "-hws",
                "15",
                "-ns",
                "30",
                "snow-ref.tif",
                "snow-def-onlyDisp.tif",
                "-od",
                ".",
                "-pre",
                "snow-grid-5j",
                "-skp",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Read the filtered file
        tsvPre = spam.helpers.readCorrelationTSV("snow-grid-5j-Guess-filtered.tsv", readError=1)
        # Get the converged nodes
        convNodes = numpy.where(tsvPre["returnStatus"] == 2)[0]
        # Read the last file
        tsvPost = spam.helpers.readCorrelationTSV("snow-grid-5j-ldic.tsv", readError=1)
        # Check that the general information is preserved
        self.assertTrue((tsvPre["fieldDims"] == tsvPost["fieldDims"]).all())
        self.assertTrue((tsvPre["fieldCoords"] == tsvPost["fieldCoords"]).all())
        # Check the converged nodes
        for node in convNodes:
            # Check returnStatus
            self.assertTrue(tsvPre["returnStatus"][node] == tsvPost["returnStatus"][node])
            # Check deltaPhiNorm
            self.assertTrue(tsvPre["deltaPhiNorm"][node] == tsvPost["deltaPhiNorm"][node])
            # Check iterations
            self.assertTrue(tsvPre["iterations"][node] == tsvPost["iterations"][node])
            # Check error
            self.assertTrue(tsvPre["error"][node] == tsvPost["error"][node])
            # Check PhiField
            self.assertTrue((tsvPre["PhiField"][node] == tsvPost["PhiField"][node]).all())

        #######################################################
        # Step 6 onto the regularStrain
        #######################################################
        # Step 6a Now let's calculate rotation vector with Geers elements (radius=1) and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "r",
                "-r",
                "1",
                "-od",
                ".",
                "-pre",
                "snow-grid-6a",
                "-tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # Now read output
        strain6a = spam.helpers.readStrainTSV("snow-grid-6a-strain-Geers.tsv")
        dims6a = [
            strain6a["fieldDims"][0],
            strain6a["fieldDims"][1],
            strain6a["fieldDims"][2],
            3,
        ]

        # Check each rotation component while having a 2-pixel crop of the boundaries
        for i in range(3):
            # print(numpy.nanmean(strain6a['r'].reshape(dims6a)[2:-2, 2:-2, 2:-2, i]))
            self.assertTrue(
                numpy.isclose(
                    numpy.nanmean(strain6a["r"].reshape(dims6a)[2:-2, 2:-2, 2:-2, i]),
                    refRotation[i],
                    atol=0.5,
                )
            )

        # Step 6b Now let's calculate small and large strains with Q8 elements and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "--Q8",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "U",
                "e",
                "vol",
                "volss",
                "dev",
                "devss",
                "-vtk",
                "-od",
                ".",
                "-pre",
                "snow-grid-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        strain6b = spam.helpers.readStrainTSV("snow-grid-6b-strain-Q8.tsv")
        dims6b = [
            strain6b["fieldDims"][0],
            strain6b["fieldDims"][1],
            strain6b["fieldDims"][2],
            3,
            3,
        ]
        # Check small strains, should be ~0
        self.assertTrue(
            numpy.allclose(
                numpy.nanmean(strain6b["e"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2)),
                numpy.zeros((3, 3)),
                atol=0.02,
            )
        )

        Umean6b = numpy.nanmean(strain6b["U"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2))
        self.assertTrue(numpy.allclose(Umean6b, numpy.eye(3), atol=0.02))

        # dev and vol both ~0
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["vol"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["volss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["dev"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["devss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )

        # VTK6b = spam.helpers.readStructuredVTK("snow-grid-6b-strain-Q8.vtk")
        # for component in ["U", "e", "vol", "volss", "dev", "devss"]:
        # self.assertTrue(numpy.nanmean(strain6b[component]), numpy.nanmean(VTK6b[component]), places=2)

        # Step 6b Now let's calculate small and large strains with Q8 elements and TSV output
        exitCode = subprocess.call(
            [
                "spam-regularStrain",
                "-raw",
                "snow-grid-5a-ldic.tsv",
                "-comp",
                "U",
                "e",
                "vol",
                "volss",
                "dev",
                "devss",
                "-vtk",
                "-od",
                ".",
                "-pre",
                "snow-grid-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        strain6b = spam.helpers.readStrainTSV("snow-grid-6b-strain-raw.tsv")
        dims6b = [
            strain6b["fieldDims"][0],
            strain6b["fieldDims"][1],
            strain6b["fieldDims"][2],
            3,
            3,
        ]
        # Check small strains, should be ~0
        self.assertTrue(
            numpy.allclose(
                numpy.nanmean(strain6b["e"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2)),
                numpy.zeros((3, 3)),
                atol=0.02,
            )
        )

        Umean6b = numpy.nanmean(strain6b["U"].reshape(dims6b)[2:-2, 2:-2, 2:-2], axis=(0, 1, 2))
        self.assertTrue(numpy.allclose(Umean6b, numpy.eye(3), atol=0.02))

        # dev and vol both ~0
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["vol"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["volss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["dev"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.nanmean(strain6b["devss"].reshape(strain6b["fieldDims"])[2:-2, 2:-2, 2:-2]),
                0,
                atol=0.02,
            )
        )

    def _test_ITKwatershed(self):
        # Run on snow data
        # load 3D image
        snowRef = spam.datasets.loadSnow()
        snowRefBin = snowRef > numpy.mean(spam.helpers.findHistogramPeaks(snowRef))

        # save it locally
        tifffile.imwrite("snow-ref-bin.tif", snowRefBin.astype("<u1") * 255)

        exitCode = subprocess.call(
            ["spam-ITKwatershed", "snow-ref-bin.tif", "-pre", "snow-ref-watershed"],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # Load result and make sure it looks like a labelled image
        lab = tifffile.imread("snow-ref-watershed.tif")
        self.assertEqual(lab.max() > 10, True)

    # def _test_gdic(self):
    # pass

    def test_deformImage(self):
        im = spam.datasets.loadSnow()[:40, :40, :40]
        tifffile.imwrite("snow-ref.tif", im)
        mask = numpy.ones(im.shape, dtype="bool")
        tifffile.imwrite("snow-mask.tif", mask)

        Phi = spam.deformation.computePhi({"r": [5.0, 0.0, 0.0], "z": [1.05, 1.0, 1.0]})

        imDef = spam.DIC.applyPhi(im, Phi=Phi)

        tifffile.imwrite("snow-def-orig.tif", imDef)

        exitCode = subprocess.call(
            ["spam-reg", "-m", "8", "-od", ".", "snow-ref.tif", "snow-def-orig.tif"],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "snow-ref-snow-def-orig-registration.tsv",
                "-im1",
                "snow-ref.tif",
                "-ns",
                "4",
                "-od",
                ".",
            ]
        )
        self.assertEqual(exitCode, 0)

        # Run the script with default pixel mode and mask
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
                "-nn",
                "4",
                "-mf2",
                "snow-mask.tif",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefPix = tifffile.imread("snow-ref-def.tif")
        self.assertTrue(numpy.abs((imDefPix - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with default pixel mode but only disp interpolate
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-disp",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
                "-nn",
                "4",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefPixDisp = tifffile.imread("snow-ref-disp-def.tif")
        self.assertTrue(numpy.abs((imDefPixDisp - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with tet mode
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-tet",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-rst",
                "1",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        imDefScript = tifffile.imread("snow-ref-tetMesh-def.tif")
        self.assertTrue(numpy.abs((imDefScript - imDef)[8:-8, 8:-8, 8:-8]).mean() < 500)

        # Run the script with tet mode and strain correction
        exitCode = subprocess.call(
            [
                "spam-deformImage",
                "-tet",
                "-pf",
                "snow-ref-snow-def-orig-registration-passed-ns4.tsv",
                "snow-ref.tif",
                "-pre",
                "snow-ref-cgs",
                "-cgs",
                "-rst",
                "1",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # imDefScriptCGS = tifffile.imread('snow-ref-cgs-tetMesh-def.tif')
        # self.assertTrue(numpy.allclose(imDefScript, imDefScriptCGS))
        # self.assertTrue(numpy.abs((imDefScriptCGS-imDef)[10:-10, 10:-10, 10:-10]).mean() < 500)

    def test_discreteDICtools(self):
        #######################################################
        # 1. Generate Data
        #######################################################
        # First we need to create some data using DEM dataset
        pixelSize = 0.0001
        blurSTD = 0.8
        noiseSTD = 0.01
        boxSizeDEM, centres, radii = spam.datasets.loadUniformDEMboxsizeCentreRadius()

        # put 0 in the middle
        centres -= numpy.mean(centres, axis=0)
        rMax = numpy.amax(radii)

        # pad box size
        boxSizeDEM = boxSizeDEM + 5 * rMax

        # add half box size to centres
        centres += numpy.array(boxSizeDEM) / 2.0
        boxSizePx = (boxSizeDEM / pixelSize).astype(int)
        centresPx = centres / pixelSize
        radiiPx = radii / pixelSize
        box = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPx,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Run watershed
        labIm0 = spam.label.ITKwatershed.watershed(box >= 0.5)
        # Save images
        tifffile.imwrite("Step0.tif", box.astype("<f4"))
        tifffile.imwrite("Lab0.tif", labIm0.astype(spam.label.labelType))

        # test of rigid translation and rotation
        # Create Phi and Apply (2 px displacement on X-axis, and 5 degree rotation along Z axis)
        # --> Change whatever you want but onyl rotations around Z-axis!!!
        translationStep1 = numpy.array([0.0, 0.0, 2.0])
        rotationStep1 = numpy.array([3.0, 0.0, 0.0])
        transformation1 = {"t": translationStep1, "r": rotationStep1}
        Phi1 = spam.deformation.computePhi(transformation1)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi1, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]

        boxDeformed = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPxDeformed,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Save images
        tifffile.imwrite("Step1.tif", boxDeformed.astype("<f4"))

        # test of rigid translation and rotation
        translationStep2 = [0, 0, 18]
        rotationStep2 = [0, 0, 0]
        transformation2 = {"t": translationStep2, "r": rotationStep2}
        Phi2 = spam.deformation.computePhi(transformation2)

        # transform centres around the centres of the box
        centresPxDeformed = numpy.zeros_like(centresPx)
        for i, centrePx in enumerate(centresPx):
            centresPxDeformed[i] = centrePx + spam.deformation.decomposePhi(Phi2, PhiPoint=centrePx, PhiCentre=numpy.array(boxSizePx) / 2.0)["t"]

        boxDeformed = spam.kalisphera.makeBlurryNoisySphere(
            boxSizePx,
            centresPxDeformed,
            radiiPx,
            blur=blurSTD,
            noise=noiseSTD,
            background=0.25,
            foreground=0.75,
        )
        # Save images
        tifffile.imwrite("Step2.tif", boxDeformed.astype("<f4"))

        del boxDeformed, centresPxDeformed, radiiPx

        #######################################################
        # 2. Run ddic -- Step0 -> Step1 with reg
        #######################################################
        # 2a registration
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(["spam-reg", "-bb", "2", "Step0.tif", "Step1.tif", "-od", "."])
        self.assertEqual(exitCode, 0)
        reg01 = spam.helpers.readCorrelationTSV("Step0-Step1-registration.tsv")
        reg01transformation = spam.deformation.decomposePhi(reg01["PhiField"][0])
        # print(reg01transformation['t'])
        # print(reg01transformation['r'])
        self.assertTrue(numpy.allclose(translationStep1, reg01transformation["t"], atol=0.05))
        self.assertTrue(numpy.allclose(rotationStep1, reg01transformation["r"], atol=0.05))

        # 2b ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "Step0-Step1-registration.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
                "-pre",
                "balls-2b",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult2b = spam.helpers.readCorrelationTSV("balls-2b-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2b["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult2b["returnStatus"]) == 2)

        # 2c now use spam-passPhiField to apply the registration to the labelled image as an input
        exitCode = subprocess.call(
            [
                "spam-passPhiField",
                "-pf",
                "Step0-Step1-registration.tsv",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-2c-reg",
                "-F",
                "rigid",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        DDICresult2c = spam.helpers.readCorrelationTSV("balls-2c-reg-passed-labelled.tsv")
        # TODO: Here you *could* check the rigid rotation's been applied to each particle
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2c["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )

        # 2d ddic from reg applied to lab
        # Repeat ddic but with this as initial guess
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-2c-reg-passed-labelled.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-od",
                ".",
                "-pre",
                "balls-2d",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult2d = spam.helpers.readCorrelationTSV("balls-2d-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult2d["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        # TODO: Here you *could* check the rigid rotation's been applied to each particle
        self.assertTrue(numpy.median(DDICresult2d["returnStatus"]) == 2)

        ########################################################
        # 3. Rerun DDIC -- Step0 -> Step1 with prev result
        ########################################################
        # 3a
        # Just run a simple DVC with no outputs except TSV + multiscale
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-2b-ddic.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step1.tif",
                "-msb",
                "2",
                "-nr",
                "-ld",
                "2",
                "-od",
                ".",
                "-pre",
                "balls-3a",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult3a = spam.helpers.readCorrelationTSV("balls-3a-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult3a["PhiField"][1:, 0, -1]),
                translationStep1[0],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult3a["returnStatus"]) == 2)

        ########################################################
        # Now check with label mover -- easier if there are no rotations
        ########################################################
        exitCode = subprocess.call(["spam-moveLabels", "Lab0.tif", "balls-3a-ddic.tsv", "-rst", "2"])
        self.assertEqual(exitCode, 0)

        imDef = tifffile.imread("Lab0-displaced.tif")
        COMref = spam.label.centresOfMass(labIm0)
        COMdef = spam.label.centresOfMass(imDef)

        # Load displaced labelled image
        labelDisp = numpy.nanmean(COMdef - COMref[0 : COMdef.shape[0]], axis=0)
        # Go from 0:COMdef.shape[0] in case any labels are lost
        self.assertAlmostEqual(translationStep1[0], labelDisp[0], places=0)
        self.assertAlmostEqual(translationStep1[1], labelDisp[1], places=0)
        self.assertAlmostEqual(translationStep1[2], labelDisp[2], places=0)

        #######################################################
        # 5a Run ddic -- Step0 -> Step2 with pixel search
        #######################################################
        # Just run a simple DVC with no outputs
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-sr",
                "{}".format(int(translationStep2[0] - 2)),
                "{}".format(int(translationStep2[0] + 2)),
                "{}".format(int(translationStep2[1] - 2)),
                "{}".format(int(translationStep2[1] + 2)),
                "{}".format(int(translationStep2[2] - 2)),
                "{}".format(int(translationStep2[2] + 2)),
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-5a",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult5a = spam.helpers.readCorrelationTSV("balls-5a-pixelSearch.tsv")
        self.assertTrue(numpy.sum(PSresult5a["returnStatus"]) == len(radii))
        # Z-displacements should be not modified by the rotation, so can be checked
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5a["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )

        # 5b ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(["spam-ddic", "-pf", "balls-5a-pixelSearch.tsv", "Step0.tif", "Lab0.tif", "Step2.tif", "-od", ".", "-pre", "balls-5b", "-o", "3"])
        self.assertEqual(exitCode, 0)

        DDICresult5b = spam.helpers.readCorrelationTSV("balls-5b-ddic.tsv")
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5b["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult5b["returnStatus"]) == 2)

        ######################################################
        # 5c Run ddic -- Step0 -> Step2 with pixel search propagate
        #######################################################
        # Just run a simple DVC with no outputs
        exitCode = subprocess.call(
            [
                "spam-pixelSearchPropagate",
                "-sr",
                "-2",
                "2",
                "-2",
                "2",
                "-2",
                "2",
                "-sp",
                "{}".format(int(COMref[1, 0])),
                "{}".format(int(COMref[1, 1])),
                "{}".format(int(COMref[1, 2])),
                "{}".format(int(translationStep2[0])),
                "{}".format(int(translationStep2[1])),
                "{}".format(int(translationStep2[2])),
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-5c",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        PSresult5c = spam.helpers.readCorrelationTSV("balls-5c-pixelSearchPropagate.tsv")
        self.assertTrue(numpy.sum(PSresult5c["returnStatus"]) == len(radii))
        # Check displacement vector
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(PSresult5c["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )

        # 5d ddic
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-5c-pixelSearchPropagate.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step2.tif",
                "-od",
                ".",
                "-pre",
                "balls-5d",
            ]
        )
        self.assertEqual(exitCode, 0)

        DDICresult5d = spam.helpers.readCorrelationTSV(
            "balls-5d-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 0, -1]),
                translationStep2[0],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 1, -1]),
                translationStep2[1],
                atol=0.3,
            )
        )
        self.assertTrue(
            numpy.isclose(
                numpy.median(DDICresult5d["PhiField"][1:, 2, -1]),
                translationStep2[2],
                atol=0.3,
            )
        )
        self.assertTrue(numpy.median(DDICresult5d["returnStatus"]) == 2)

        # 5e ddic result passed to same label image with spam-passPhiField (ideally it would be differently labelled)
        # ...

        #######################################################
        # 6. Check -skp
        #######################################################
        # Manually modify the RS of the TSV from 5d
        DDICresult5d["returnStatus"][-3:] = -3
        # Save the new TSV2
        outMatrix = numpy.array(
            [
                numpy.array(range(DDICresult5d["numberOfLabels"])),
                DDICresult5d["fieldCoords"][:, 0],
                DDICresult5d["fieldCoords"][:, 1],
                DDICresult5d["fieldCoords"][:, 2],
                DDICresult5d["PhiField"][:, 0, 3],
                DDICresult5d["PhiField"][:, 1, 3],
                DDICresult5d["PhiField"][:, 2, 3],
                DDICresult5d["PhiField"][:, 0, 0],
                DDICresult5d["PhiField"][:, 0, 1],
                DDICresult5d["PhiField"][:, 0, 2],
                DDICresult5d["PhiField"][:, 1, 0],
                DDICresult5d["PhiField"][:, 1, 1],
                DDICresult5d["PhiField"][:, 1, 2],
                DDICresult5d["PhiField"][:, 2, 0],
                DDICresult5d["PhiField"][:, 2, 1],
                DDICresult5d["PhiField"][:, 2, 2],
                DDICresult5d["pixelSearchCC"],
                DDICresult5d["error"],
                DDICresult5d["iterations"],
                DDICresult5d["returnStatus"],
                DDICresult5d["deltaPhiNorm"],
                DDICresult5d["LabelDilate"],
            ]
        ).T
        numpy.savetxt(
            "balls-6a-ddic.tsv",
            outMatrix,
            fmt="%.7f",
            delimiter="\t",
            newline="\n",
            comments="",
            header="Label\tZpos\tYpos\tXpos\t"
            + "Zdisp\tYdisp\tXdisp\t"
            + "Fzz\tFzy\tFzx\t"
            + "Fyz\tFyy\tFyx\t"
            + "Fxz\tFxy\tFxx\t"
            + "PSCC\terror\titerations\treturnStatus\tdeltaPhiNorm\tLabelDilate",
        )
        # Run
        # Just run a simple DVC with no outputs except TSV
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "-pf",
                "balls-6a-ddic.tsv",
                "Step0.tif",
                "Lab0.tif",
                "Step2.tif",
                "-od",
                ".",
                "-skp",
                "-pre",
                "balls-6b",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        # Read both TSV files
        DDICresult6a = spam.helpers.readCorrelationTSV(
            "balls-6a-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        DDICresult6b = spam.helpers.readCorrelationTSV(
            "balls-6b-ddic.tsv",
            readConvergence=True,
            readError=True,
            readLabelDilate=True,
            readPixelSearchCC=True,
        )
        # Check that both files are identical for the converged grains of DDICresult6a
        self.assertTrue(DDICresult6a["fieldDims"] == DDICresult6b["fieldDims"])
        self.assertTrue((DDICresult6a["fieldCoords"] == DDICresult6b["fieldCoords"]).all())
        self.assertTrue(DDICresult6a["numberOfLabels"] == DDICresult6b["numberOfLabels"])
        convLabDDICresult6a = numpy.where(DDICresult6a["returnStatus"] == 2)[0]
        # Loop through each label and verify each key
        for label in convLabDDICresult6a:
            # Check returnStatus
            self.assertTrue(DDICresult6a["returnStatus"][label] == DDICresult6b["returnStatus"][label])
            # Check deltaPhiNorm
            self.assertTrue(DDICresult6a["deltaPhiNorm"][label] == DDICresult6b["deltaPhiNorm"][label])
            # Check iterations
            self.assertTrue(DDICresult6a["iterations"][label] == DDICresult6b["iterations"][label])
            # Check error
            self.assertTrue(DDICresult6a["error"][label] == DDICresult6b["error"][label])
            # Check pixelSearchCC
            self.assertTrue(DDICresult6a["pixelSearchCC"][label] == DDICresult6b["pixelSearchCC"][label])
            # Chekc LabelDilate
            self.assertTrue(DDICresult6a["LabelDilate"][label] == DDICresult6b["LabelDilate"][label])
            # Check PhiField
            self.assertTrue((DDICresult6a["PhiField"][label] == DDICresult6b["PhiField"][label]).all())

        #######################################################
        # 7. Rerun ddic -- Step0 -> Step2 with prev result
        #######################################################
        # 7a repeat pixelSearch to give exactly same result
        exitCode = subprocess.call(
            [
                "spam-pixelSearch",
                "-pf",
                "balls-5a-pixelSearch.tsv",
                "-sr",
                "-3",
                "3",
                "-3",
                "3",
                "-3",
                "3",
                "Step0.tif",
                "Step2.tif",
                "-lab1",
                "Lab0.tif",
                "-od",
                ".",
                "-pre",
                "balls-7a",
            ]
        )
        PSresult6a = spam.helpers.readCorrelationTSV("balls-7a-pixelSearch.tsv")
        self.assertTrue(numpy.sum(PSresult6a["returnStatus"]) == len(radii))
        # Al displacements should be exactly the same as before
        # print(PSresult5a['PhiField'][1:,0:3,-1])
        # print(PSresult6a['PhiField'][1:,0:3,-1])
        self.assertTrue(0 == numpy.sum(PSresult5a["PhiField"][1:, 0:3, -1] - PSresult6a["PhiField"][1:, 0:3, -1]))

        #######################################################
        # 4. 2020-09-05 EA and OS: New "extreme" test with particles touching the boundaries
        #######################################################
        im1 = spam.kalisphera.makeBlurryNoisySphere(
            [100, 100, 100],
            [[50, 50, 50], [50, 10, 10]],
            [10, 10],
            0.5,
            0.01,
            background=0.0,
            foreground=1.0,
        )
        im2 = spam.kalisphera.makeBlurryNoisySphere(
            [100, 100, 100],
            [[54, 54, 54], [54, 14, 14]],
            [10, 10],
            0.5,
            0.01,
            background=0.0,
            foreground=1.0,
        )
        im1lab = scipy.ndimage.label(im1 > 0.5)[0]

        tifffile.imwrite("extreme-im1.tif", im1)
        tifffile.imwrite("extreme-im1lab.tif", im1lab)
        tifffile.imwrite("extreme-im2.tif", im2)

        # Just run a simple DVC
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "extreme-im1.tif",
                "extreme-im1lab.tif",
                "extreme-im2.tif",
                "-m",
                "10",
                "-ld",
                "2",
            ]
        )
        self.assertEqual(exitCode, 0)
        TSVextreme = spam.helpers.readCorrelationTSV("extreme-im1-extreme-im2-ddic.tsv")
        # self.assertAlmostEqual(TSV['PhiField'][i][u,-1], TSV2['PhiField'][i][u,-1], places=1)
        self.assertEqual(list(TSVextreme["returnStatus"][1:]), [2, 2])
        for axis in range(3):
            for label in [1, 2]:
                self.assertAlmostEqual(
                    numpy.abs(TSVextreme["PhiField"][label, axis, -1] - 4.0),
                    0.0,
                    places=1,
                )

        #######################################################
        # 5. 2D DDIC - simple version
        #######################################################
        # Create images
        imGrey1 = spam.kalisphera.makeBlurryNoisySpheroid([40, 80, 40], [[20, 40, 20]], [[10, 20]], [[0, 1, 0]], blur=0.8, noise=0.03)
        imGrey2 = spam.kalisphera.makeBlurryNoisySpheroid(
            [40, 80, 40],
            [[20, 45, 20]],
            [[10, 20]],
            [[0, 0.96, 0.28]],
            blur=0.8,
            noise=0.03,
        )
        # Create 2D images
        imGrey1 = imGrey1[19, :, :]
        imGrey2 = imGrey2[19, :, :]
        imLab = (imGrey1 > 0.5).astype(int)
        tifffile.imwrite("imGrey1_2D.tif", imGrey1.astype(numpy.float32))
        tifffile.imwrite("imGrey2_2D.tif", imGrey2.astype(numpy.float32))
        tifffile.imwrite("imLab_2D.tif", imLab.astype("<u2"))
        # Run DDIC
        exitCode = subprocess.call(
            [
                "spam-ddic",
                "imGrey1_2D.tif",
                "imLab_2D.tif",
                "imGrey2_2D.tif",
                "-od",
                ".",
                "-pre",
                "2D",
            ]
        )
        self.assertEqual(exitCode, 0)
        res = spam.helpers.readCorrelationTSV("2D-ddic.tsv")
        res1transformation = spam.deformation.decomposePhi(res["PhiField"][1])
        magDisp = numpy.linalg.norm(res1transformation["t"])
        numpy.linalg.norm(res1transformation["r"])
        self.assertLessEqual(numpy.abs(magDisp - 5), 2)

    def test_discreteStrain(self):
        # make sure it runs the help without error
        exitCode = subprocess.call(["spam-discreteStrain", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        nPoints = 30
        points = numpy.random.randint(low=0, high=100, size=(nPoints, 3)).astype("<f4")
        pointsDef = points * 1.01
        displacements = pointsDef - points
        radii = numpy.ones(nPoints)
        tri = spam.mesh.triangulate(points)

        spam.helpers.writeUnstructuredVTK(
            points,
            tri,
            pointData={"radius": radii, "displacements": displacements},
            fileName="test.vtk",
        )

        exitCode = subprocess.call(
            [
                "spam-discreteStrain",
                "test.vtk",
                "-pre",
                "test-strain",
                "-comp",
                "dev",
                "vol",
                "U",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)
        VTK = spam.helpers.readUnstructuredVTK("test-strain.vtk")
        self.assertAlmostEqual(VTK[3]["dev"].mean(), 0, places=3)
        self.assertAlmostEqual(VTK[3]["vol"].mean(), 3 * (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['xx'].mean(), (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['yy'].mean(), (1.01 - 1.00), places=3)
        # self.assertAlmostEqual(VTK[3]['zz'].mean(), (1.01 - 1.00), places=3)

    def test_all_help(self):
        FNULL = open(os.devnull, "w")
        exitCode = subprocess.call(["spam-reg", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-pixelSearch", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-pixelSearchPropagate", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-ldic", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-passPhiField", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-regularStrain", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-gdic", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-deformImage", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-ITKwatershed", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-filterPhiField", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        exitCode = subprocess.call(["spam-mesh", "--help"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

    def test_grf(self):
        FNULL = open(os.devnull, "w")
        exitCode = subprocess.call(["spam-grf"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

    def test_mesh(self):
        FNULL = open(os.devnull, "w")
        exitCode = subprocess.call(
            [
                "spam-mesh",
                "-pre",
                "test-mesh",
                "-cube",
                "0.1",
                "0.4",
                "0.2",
                "0.3",
                "0",
                "1.0",
                "-lc",
                "0.1",
                "-h5",
            ],
            stdout=FNULL,
            stderr=FNULL,
        )
        self.assertEqual(exitCode, 0)

        # read hdf5 with script
        exitCode = subprocess.call(["spam-hdf-reader", "test-mesh-lc0.1.h5"], stdout=FNULL, stderr=FNULL)
        self.assertEqual(exitCode, 0)

        # check what's inside the hd5 file
        with h5py.File("test-mesh-lc0.1.h5") as f:
            self.assertEqual(f["mesh-points"].shape[1], 3)
            self.assertEqual(f["mesh-connectivity"].shape[1], 4)
            self.assertEqual(min(f["mesh-connectivity"][:].ravel()), 0)
            self.assertEqual(max(f["mesh-connectivity"][:].ravel()), f["mesh-points"].shape[0] - 1)
            for i in range(3):
                self.assertAlmostEqual(numpy.max(f["mesh-points"][:], axis=0)[i], [0.4, 0.3, 1.0][i])
                self.assertAlmostEqual(numpy.min(f["mesh-points"][:], axis=0)[i], [0.1, 0.2, 0.0][i])

    def test_mesh_subdomains(self):
        open(os.devnull, "w")
        exitCode = subprocess.call(
            [
                "spam-mesh-subdomains",
                "-pre",
                "test-mesh",
                "-cube",
                "0.1",
                "0.4",
                "0.2",
                "0.3",
                "0",
                "1.0",
                "-lc1",
                "0.05",
                "-r",
                "1",
                "2",
                "3",
                "-msh",
                "-v",
                "0",
                "-vtk",
            ]
        )
        self.assertEqual(exitCode, 0)


if __name__ == "__main__":
    unittest.main()
