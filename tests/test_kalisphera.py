# -*- coding: utf-8 -*-

import unittest
import os
import tifffile
import numpy

import spam.datasets
import spam.label
import spam.kalisphera

VERBOSE = True

class TestAll(spam.helpers.TestSpam):

    def test_kalisphera(self):
        DIMS = (50,50,50)
        im = numpy.zeros(DIMS, dtype='<f8')

        # randomly choose radius between 5 and 20
        rad = 5 + 15.0*numpy.random.rand()

        theoreticalVolume = (4./3.)*numpy.pi*rad**3

        ret = spam.kalisphera.makeSphere(im, numpy.array(DIMS)/2., rad)
        self.assertEqual(ret, 0)

        # Check volume of sphere
        # Apparently places is after to dot, so take the ratio
        self.assertAlmostEqual(numpy.sum(im)/theoreticalVolume, 1.0, places=2)

        # make sure it complains with 2D vol:
        ret = spam.kalisphera.makeSphere(im[im.shape[0]//2], numpy.array(DIMS)/2., rad)
        self.assertEqual(ret, -1)

        # make sure it complains with different number of centres and radii:
        ret = spam.kalisphera.makeSphere(im, [ numpy.array(DIMS)/2. ], [ rad, 10., 15. ])
        self.assertEqual(ret, -1)

        # Also test the noisy blurry version, overwrite im
        im = spam.kalisphera.makeBlurryNoisySphere((50, 50, 50), [25, 25, 25], 15, 0.5, 0.05, background=0.0, foreground=1.0)
        self.assertEqual(numpy.abs(numpy.mean(im[20:30, 20:30, 20:30]) - 1.0) < 0.1, True)
        self.assertEqual(numpy.abs(numpy.mean(im[ 0:10,  0:10,  0:10]) - 0.0) < 0.1, True)

        # check centre is OK
        cin  = [30,30,30]
        cout = spam.label.centresOfMass(spam.kalisphera.makeBlurryNoisySphere([100,100,100], [30,30,30], [10]) > 0.5)[1]
        self.assertAlmostEqual(cin, cout.tolist(), places=3)


    def test_DEM_dataset(self):
        # as per Kalisphera example
        # m/pixel
        pixelSize = 60.e-6
        # The standard deviation of the image blurring to be applied first
        blurSTD = 0.8
        # The standard deviation of the random noise to be added to each voxel
        noiseSTD = 0.03
        boxSizeDEM, centres, radii = spam.datasets.loadDEMboxsizeCentreRadius()
        # get maximum radius to pad our image (periodic boundaries...)
        rMax = numpy.amax(radii)
        boxSize = boxSizeDEM + 3 * rMax
        # move the positions to the new center of the image
        centres[:, :] = centres[:, :] + 1.5 * rMax
        # turn the mm measures into pixels
        boxSize = int(numpy.ceil(numpy.max(boxSize[:]) / pixelSize))
        centres = centres / pixelSize
        radii = radii / pixelSize
        Box = numpy.zeros((boxSize, boxSize, boxSize), dtype="<f8")
        spam.kalisphera.makeSphere(Box, centres, radii)
        theoreticalVolume = numpy.sum((4./3.)*numpy.pi*radii**3)
        self.assertAlmostEqual(numpy.sum(Box)/theoreticalVolume, 1.0, places=2)


    def test_spheroid(self):
        DIMS = (50,50,50)

        # start with a noisy blurry sphere and check volume
        sphereRadius = 20
        sphereTheoreticalVolume = (4/3)*numpy.pi*sphereRadius**3
        sphere = spam.kalisphera.makeBlurryNoisySpheroid(DIMS, numpy.array(DIMS)/2, [sphereRadius,sphereRadius], [1,0,0], blur=0.5, noise=0.01, background=0.25, foreground=0.75)
        sphereVolume = numpy.sum(sphere > 0.5)
        self.assertTrue(numpy.isclose(sphereVolume/sphereTheoreticalVolume, 1, atol=0.2))

        # Crash outside the edges, centre at 0
        sphereEighth = spam.kalisphera.makeBlurryNoisySpheroid(DIMS, [0,0,0], [sphereRadius,sphereRadius], [1,0,0], blur=0, noise=0, background=0.0, foreground=1.0)
        self.assertTrue(numpy.isclose(sphereEighth.sum()/sphereTheoreticalVolume, 0.125, atol=0.05))

        # Crash outside the edges, centre at opposite edge
        sphereEighth = spam.kalisphera.makeBlurryNoisySpheroid(DIMS, DIMS, [sphereRadius,sphereRadius], [1,0,0], blur=0, noise=0, background=0.0, foreground=1.0)
        self.assertTrue(numpy.isclose(sphereEighth.sum()/sphereTheoreticalVolume, 0.125, atol=0.05))

        # Let's make two objects -- use the two corner cases
        sphereTwoEighths = spam.kalisphera.makeBlurryNoisySpheroid(DIMS,
                                                                   [[0,0,0], DIMS],
                                                                   [[sphereRadius,sphereRadius],[sphereRadius,sphereRadius]],
                                                                   [[1,0,0], [1,0,0]],
                                                                   blur=0,
                                                                   noise=0,
                                                                   background=0.0,
                                                                   foreground=1.0)
        self.assertTrue(numpy.isclose(sphereTwoEighths.sum()/sphereTheoreticalVolume, 0.25, atol=0.05))

        # Let's actually make a spheroid... Lentils first (always) -- means A>C
        lentil = spam.kalisphera.makeBlurryNoisySpheroid(DIMS,
                                                         numpy.array(DIMS)/2,
                                                         [20,10],
                                                         [1,0,0], blur=0, noise=0, background=0.0, foreground=1.0)
        lentilTheoreticalVolume = (4/3)*numpy.pi*(20*20*10)
        self.assertTrue(numpy.isclose(lentil.sum()/lentilTheoreticalVolume, 1, atol=0.1))

        # check orientation, max eigenvector for a lentil
        orientation = spam.label.momentOfInertia(lentil > 0.5)[1][1, 0:3]
        self.assertTrue(numpy.allclose(numpy.abs(orientation), [1,0,0], atol=0.1))

        ellipseAxes = spam.label.ellipseAxes(lentil > 0.5)[1, 0:3]
        self.assertTrue(numpy.allclose(ellipseAxes , [20,20,10], atol=0.1))


if __name__ == '__main__':
        unittest.main()
