# -*- coding: utf-8 -*-

import unittest
import numpy
import os

import spam.measurements
import spam.mesh
import spam.datasets


class TestAll(spam.helpers.TestSpam):

    def test_volume(self):
        cyl = spam.mesh.createCylindricalMask((100, 100, 100), radius=50)
        segmented = numpy.array(spam.datasets.loadSnow() > 27000) + 1

        # should be 537412
        vol = spam.measurements.volume(segmented, phase=2)
        self.assertEqual(vol, 537412)

        # should be 426558
        vol = spam.measurements.volume(segmented, phase=2, ROI=cyl)
        self.assertEqual(vol, 426558)

        # should be 0.5426946564885496
        vol = spam.measurements.volume(segmented, phase=2, ROI=cyl, specific=True)
        self.assertAlmostEqual(vol, 0.426558, places=5)

        # phase 0
        vol = spam.measurements.volume(segmented, phase=0)

    def test_surfaceAreaAndTotalCurvature(self):
        import spam.kalisphera
        sphere = numpy.zeros((100, 100, 100))
        spam.kalisphera.makeSphere(sphere, numpy.array([50.0, 50.0, 50.0]).astype('<f8'), float(30.0))
        vol = 4.0 * (numpy.pi * 30.0**3.0) / 3.0
        surfFunction = spam.measurements.surfaceArea(sphere, level=0.5)
        self.assertAlmostEqual(surfFunction / vol, 0.1, places=2)

        spam.kalisphera.makeSphere(sphere, numpy.array([5.0, 5.0, 5.0]).astype('<f8'), float(30.0))
        spam.measurements.totalCurvature(sphere, level=0.5)
        spam.measurements.totalCurvature(sphere, level=0.5, getMeshValues=True, fileName="spam.vtk", stepSize=2)

    def test_eulerCharacteristic(self):
        cyl = spam.mesh.createCylindricalMask((100, 100, 100), radius=50)
        segmented = numpy.array(spam.datasets.loadSnow() > 27000) + 1

        # should be -668
        vol = spam.measurements.eulerCharacteristic(segmented, phase=2)
        self.assertEqual(vol, -668)

        # should be -1388
        vol = spam.measurements.eulerCharacteristic(segmented, phase=1, ROI=cyl)
        self.assertEqual(vol, -1388)

        # phase 0
        vol = spam.measurements.eulerCharacteristic(segmented, phase=0)

    def test_perimeter(self):
        cyl = spam.mesh.createCylindricalMask((100, 100, 100), radius=50)[:, :, 25]
        segmented = numpy.array(spam.datasets.loadSnow() > 27000)[:, :, 25] + 1

        spam.measurements.perimeter(segmented, phase=0)
        spam.measurements.perimeter(segmented, phase=1)
        spam.measurements.perimeter(segmented, phase=1, ROI=cyl)

    def test_generic(self):
        # just to to test if they all run dim 3
        cyl = spam.mesh.createCylindricalMask((10, 10, 10), radius=5)
        spam.measurements.generic(cyl, 0, phase=1, ROI=cyl, verbose=True)
        spam.measurements.generic(cyl, 1)
        spam.measurements.generic(cyl, 2, level=0, aspectRatio=(1.0, 1.0, 1.0))
        spam.measurements.generic(cyl, 3, phase=1, aspectRatio=(1.0, 1.0, 1.0), ROI=cyl, specific=True)
        # dim 2
        cyl = cyl[:, :, 5]
        spam.measurements.generic(cyl, 0, phase=1, ROI=cyl, verbose=True)
        spam.measurements.generic(cyl, 1, phase=1, aspectRatio=(1.0, 1.0), ROI=cyl)
        spam.measurements.generic(cyl, 2, phase=1, ROI=cyl, specific=True)
        # dim 1
        cyl = cyl[:, 5]
        spam.measurements.generic(cyl, 0, phase=1, ROI=cyl, verbose=True)
        spam.measurements.generic(cyl, 1, phase=1, ROI=cyl, specific=True)

        seg = numpy.random.rand(3, 3, 3) > 0.5
        spam.measurements.generic(seg, 4, phase=1)
        seg = numpy.random.rand(3, 3, 3, 3) > 0.5
        spam.measurements.generic(seg, 0, phase=1)


if __name__ == '__main__':
    unittest.main()
