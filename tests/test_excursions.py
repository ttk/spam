# -*- coding: utf-8 -*-

import unittest
import numpy
import os

import spam.excursions
import spam.helpers


class TestAll(spam.helpers.TestSpam):

    def test_excursions(self):
        rf = spam.excursions.simulateRandomField(lengths=(1.0, 2.0, 3.0), nNodes=(3, 4, 5),
                                                 dim=3, nRea=2, tifFile='tmp', vtkFile='tmp', fieldFile='tmp')
        self.assertEqual(rf.shape[0], 3)
        self.assertEqual(rf.shape[1], 4)
        self.assertEqual(rf.shape[2], 5)
        self.assertEqual(rf.shape[3], 2)
        rfRead = spam.helpers.readStructuredVTK('tmp_00001.vtk')['RandomField']
        self.assertAlmostEqual(numpy.array(rfRead-rf[:, :, :, 1].ravel()).sum(), 0.0, places=5)

        # all covariance
        covariance = {'type': 'stable'}
        spam.excursions.simulateRandomField(dim=1, covariance=covariance)
        covariance = {'type': 'matern'}
        spam.excursions.simulateRandomField(dim=1, covariance=covariance)
        covariance = {'type': 'bessel'}
        spam.excursions.simulateRandomField(dim=1, covariance=covariance)
        covariance = {'type': 'dampedcos'}
        spam.excursions.simulateRandomField(dim=1, covariance=covariance)
        covariance = {'type': 'sinepower'}
        method = 'circulant'
        spam.excursions.simulateRandomField(dim=1, covariance=covariance, method=method)

        # manual inputs
        covariance = 'RMstable(var=1.0, scale=1.0, alpha=2)'
        data = 'c(0, 0)'
        given = 'c(0, 1)'
        spam.excursions.simulateRandomField(dim=1, covariance=covariance, data=data, given=given)

        # tif files and im
        spam.excursions.simulateRandomField(dim=1, tifFile='tmp', verbose=True)
        spam.excursions.simulateRandomField(dim=2, tifFile='tmp')
        spam.excursions.simulateRandomField(dim=2, tifFile='tmp', nRea=2, verbose=True)
        spam.excursions.simulateRandomField(dim=3, tifFile='tmp', fieldFile='tmp', vtkFile='tmp', verbose=True)
        spam.excursions.simulateRandomField(dim=3, tifFile='tmp', fieldFile='tmp', nRea=2)

        # parameters functions
        spam.excursions.parametersLogToGauss(1, 1)
        uni = spam.excursions.fieldGaussToUniform(rfRead)
        gau = spam.excursions.fieldUniformToGauss(uni)
        self.assertAlmostEqual(numpy.array(rfRead-gau).sum(), 0.0, places=5)

    def test_elkc(self):
        spam.excursions.expectedMesures(0, 1, 2)
        spam.excursions.gaussianMinkowskiFunctionals(1, 1, distributionType='log', hittingSet='head')
        spam.excursions.gaussianMinkowskiFunctionals(0, 1, hittingSet='unknown')
        spam.excursions.gaussianMinkowskiFunctionals(1, 1, distributionType='unknown')
        spam.excursions.gaussianMinkowskiFunctionals(0, -1)
        spam.excursions.secondSpectralMoment(1, 1)
        spam.excursions.secondSpectralMoment(1, 1, correlationType='matern')
        spam.excursions.secondSpectralMoment(1, 1, correlationType='matern', nu=0.5)
        spam.excursions.secondSpectralMoment(1, 1, correlationType='unknown')
        spam.excursions.flag(1, 1)
        spam.excursions.flag(1, 2)
        spam.excursions.hermitePolynomial(0, 1)
        spam.excursions.ballVolume(3)
        spam.excursions.lkcCuboid(1, 2, 1)
        spam.excursions.lkcCuboid(2, 1, 1)
        spam.excursions.lkcCuboid(1, 2, [1, 1, 1])
        spam.excursions.lkcCuboid(1, 2, [1, 1])
        spam.excursions.lkcCuboid(1, 3, [1, 1, 1])
        spam.excursions.lkcCuboid(1, 4, [1, 1, 1, 1])

if __name__ == '__main__':
    unittest.main()
