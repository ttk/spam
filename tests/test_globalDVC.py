import unittest

import numpy
import spam.datasets
import spam.deformation
import spam.DIC
import spam.filters
import spam.helpers
import spam.kalisphera
import spam.label
import spam.mesh

numpy.set_printoptions(precision=3)


class TestAll(spam.helpers.TestSpam):
    def test_globalCorrelation(self):

        # load reference image
        snowRef = spam.datasets.loadSnow()

        # create mesh
        margin = [5, 10, 10]
        meshCharacteristicLength = 15
        points, connectivity = spam.mesh.createCuboid(
            [
                snowRef.shape[0] - 2 * margin[0],
                snowRef.shape[1] - 2 * margin[1],
                snowRef.shape[2] - 2 * margin[2],
            ],  # lengths
            meshCharacteristicLength,
            origin=margin,
        )

        # test 1: simple translation
        transformation = {"t": [1.3, 0.5, -0.1]}
        Phi = spam.deformation.computePhi(transformation)
        snowDef = spam.DIC.applyPhi(snowRef, Phi=Phi)

        # 1.1: no initial displacement
        displacements = spam.DIC.globalCorrelation(
            snowRef,
            snowDef,
            points,
            connectivity,
            maxIterations=10,
            # debugFiles=True
        )

        # remove noisy displacements from the border
        # borders = [
        #     [points[:, i].min() for i in range(3)],  # min x, y, z
        #     [points[:, i].max() for i in range(3)],  # max x, y, z
        # ]
        #
        # disp_bulk = []
        #
        # def is_point_on_border(point, borders):
        #     is_min = [point[i] - 1e-6 <= borders[0][i] for i in range(3)]
        #     is_max = [point[i] + 1e-6 >= borders[1][i] for i in range(3)]
        #     return any(is_min + is_max)
        #
        # for disp, point in zip(displacements, points):
        #
        #     if is_point_on_border(point, borders):
        #         continue
        #
        #     # node note on border
        #     disp_bulk.append(disp)

        # test median displacements matches imposed translation
        medians = numpy.median(displacements, axis=0)
        self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))

        # 1.2: with initial displacement (half the solution)
        initialDisplacements = numpy.zeros_like(points)
        for i in range(3):
            initialDisplacements[i] = 0.5 * transformation["t"][i]
        displacements = spam.DIC.globalCorrelation(
            snowRef,
            snowDef,
            points,
            connectivity,
            initialDisplacements=initialDisplacements,
        )
        # test median displacements matches imposed translation
        medians = numpy.median(displacements, axis=0)
        self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))

        # test 2: test bulk regularisation
        regularisation = {
            "MESH": {"type": "cuboid"},
            "BULK": {"young": 25, "poisson": 0.25, "ksi": 2},
            "DIRICHLET": {"ksi": {"z_start": 2, "z_end": 2, "y_start": None}},
        }

        displacements = spam.DIC.globalCorrelation(
            snowRef,
            snowDef,
            points,
            connectivity,
            regularisation=regularisation,
        )
        # test median displacements matches imposed translation
        medians = numpy.median(displacements, axis=0)
        self.assertTrue(numpy.allclose(medians, transformation["t"], rtol=1e-2))

        spam.helpers.writeUnstructuredVTK(points, connectivity, pointData={"displacements": displacements})


if __name__ == "__main__":
    spam.helpers.TestSpam.DEBUG = True
    unittest.main()
