# -*- coding: utf-8 -*-

import unittest
import numpy

import spam.measurements
import spam.datasets
import spam.plotting
import spam.kalisphera

class TestAll(spam.helpers.TestSpam):

    # Test for different initial guesses in image with 2 phases
    def test_findHistogramPeaks(self):

        im = spam.datasets.loadSnow()
        guess1 = 20000
        guess2 = 13000
        # 2. check that we're not too sensitive to the initial guess....
        res1 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=2, gaussianFit=False, mask=False)
        res2 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess2, valley2=[],phases=2, gaussianFit=False, mask=False)
        self.assertEqual(res1[0], res2[0])
        self.assertEqual(res1[1], res2[1])
        # 3. Check that gaussian fit is not a million miles away from the simple peak selection
        res3 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=2, gaussianFit=True, mask=False)
        self.assertAlmostEqual(float(res3[0])/65535.0, float(res2[0])/65535.0, places=2)
        self.assertAlmostEqual(float(res3[1])/65535.0, float(res2[1])/65535.0, places=2)

        # 4. Check that it returns sigmas
        im = spam.datasets.loadSnow()
        guess1 = 20000
        res1 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=2, gaussianFit=True, mask=False, returnSigma = True)
        self.assertEqual(type(res1), tuple)
        # Check that it avoids blockage when gaussianFit is off and returnSigma on
        im = spam.datasets.loadSnow()
        guess1 = 20000
        res1 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=2, gaussianFit=False, mask=False, returnSigma = True)
        self.assertIsNot(res1   , None)


        # 5. test that the fitting of the two gaussians fits the histogram well (i.e., subtract histogram from fit1+fit2)
        #res5, sig5 = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=2, gaussianFit=True, mask=False, returnSigma=True)
        #print(res5, "and..", sig5)
        #reshist = spam.plotting.greyLevelHistogram.plotGreyvalueHistogram(im, greyRange=[0, 65535], bins=256)
        #print(reshist)

        # 6. test a mask that excludes a 20px border
        imMasked = im.copy()
        imMasked[80:] = 0
        res6 = spam.helpers.histogramTools.findHistogramPeaks(imMasked, valley1=guess1, valley2=[], phases=2, gaussianFit=True, mask=True)
        self.assertAlmostEqual(float(res6[0])/65535.0, float(res3[0])/65535.0, places=3)
        self.assertAlmostEqual(float(res6[1])/65535.0, float(res3[1])/65535.0, places=3)

        # 7. try to make a three-phase one
        vol = numpy.zeros((100,100,100), dtype='<f4')
        spam.kalisphera.makeSphere(vol, (50, 75, 50), 22)
        vol *= 2
        spam.kalisphera.makeSphere(vol, (50, 20, 50), 18)
        vol += numpy.random.normal(size=(100, 100, 100), scale=0.05)

        # Check without Gauss fitting
        peakVals = spam.helpers.histogramTools.findHistogramPeaks(vol, valley1=0.5, valley2=1.5, phases=3, gaussianFit=False, greyRange=[-2,5])
        self.assertAlmostEqual(float(peakVals[0]), 0, places=1)
        self.assertAlmostEqual(float(peakVals[1]), 1, places=1)
        self.assertAlmostEqual(float(peakVals[2]), 2, places=1)

        peakVals = spam.helpers.histogramTools.findHistogramPeaks(vol, valley1=0.5, valley2=1.5, phases=3, gaussianFit=True, greyRange=[-2,5])
        self.assertAlmostEqual(float(peakVals[0]), 0, places=1)
        self.assertAlmostEqual(float(peakVals[1]), 1, places=1)
        self.assertAlmostEqual(float(peakVals[2]), 2, places=1)

        # Check that the valley2 > valley 1
        im = spam.datasets.loadSnow()
        guess1 = 30000
        guess2 = 20000
        res = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=guess2,phases=3, mask=False)
        self.assertEqual(res,None)

        # Check that it does not fail when the image is too small and gaussianFit = True
        im = spam.kalisphera.makeBlurryNoisySphere([30,30,30],[15,15,15], [15])
        res = spam.helpers.histogramTools.findHistogramPeaks(im, valley1 = 0.5, greyRange = [0, 1], gaussianFit = False)
        self.assertIsNot(res, None)


# Test for choosing wrong the number of phases
    def test_findingPeaks1(self):
        im = spam.datasets.loadSnow()
        guess1 = 20000
        res = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=1, mask=False)
        self.assertEqual(res,None)

    def test_findingPeaks2(self):
        im = spam.datasets.loadSnow()
        guess1 = 20000
        res = spam.helpers.histogramTools.findHistogramPeaks(im,valley1=guess1, valley2=[],phases=6, mask=False)
        self.assertEqual(res,None)

    def test_histogramNorm(self):
        # 1. take the snow, normalise it
        im = spam.datasets.loadSnow()
        guess1 = 20000
        peaks1 = spam.helpers.histogramTools.findHistogramPeaks(im, valley1=guess1, phases=2, gaussianFit=False, mask=False)
        im2 = spam.helpers.histogramTools.histogramNorm(im, [peaks1[0],peaks1[1]], [0.25, 0.75])
        peaks2 = spam.helpers.histogramTools.findHistogramPeaks(im2, valley1=0.5, phases=2, gaussianFit=True, mask=False, greyRange=[0,1])

        # 2. refit-it nd make sure peaks are at 0.25 and 0.75
        self.assertAlmostEqual(peaks2[0],0.25, places =1)
        self.assertAlmostEqual(peaks2[1],0.75, places =1)
        # 3. Check if the number of peaks is different than 2
        res31 = spam.helpers.histogramNorm(im, [1])
        res32 = spam.helpers.histogramNorm(im, [1,2,3])
        self.assertEqual(res31,None)
        self.assertEqual(res32,None)

        # Check that catched error when peak 1 > peak 2 and p1 > p2
        im = spam.datasets.loadSnow()
        guess1 = 20000
        peaks1 = spam.helpers.histogramTools.findHistogramPeaks(im, valley1=guess1, phases=2, gaussianFit=False, mask=False)
        im2 = spam.helpers.histogramTools.histogramNorm(im, [peaks1[1],peaks1[0]], [0.25, 0.75])
        im3 = spam.helpers.histogramTools.histogramNorm(im, [peaks1[1],peaks1[0]], [0.75, 0.25])
        self.assertEqual(im2,None)
        self.assertEqual(im3,None)

        pass

if __name__ == '__main__':
        unittest.main()
