#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

namespace py = pybind11;

void applyPhi(py::array_t<float> im,
              py::array_t<float> imDef,
              py::array_t<float> Finv,
              py::array_t<float> Fpoint,
              int interpOrder);

void binningFloat(py::array_t<float> im,
                  py::array_t<float> imBin,
                  py::array_t<int> offset,
                  int binning);
void binningUInt(py::array_t<unsigned short> im,
                 py::array_t<unsigned short> imBin,
                 py::array_t<int> offset,
                 int binning);
void binningChar(py::array_t<unsigned char> im,
                 py::array_t<unsigned char> imBin,
                 py::array_t<int> offset,
                 int binning);

void computeDICoperators(py::array_t<float> im1,
                         py::array_t<float> im2,
                         py::array_t<float> im2gz,
                         py::array_t<float> im2gy,
                         py::array_t<float> im2gx,
                         py::array_t<double> M,
                         py::array_t<double> A);
void computeDICjacobian(py::array_t<float> im1,
                        py::array_t<float> im2,
                        py::array_t<float> im2gz,
                        py::array_t<float> im2gy,
                        py::array_t<float> im2gx,
                        py::array_t<double> A);
void computeDICoperatorsGM(py::array_t<float> im1,
                           py::array_t<float> im2,
                           py::array_t<float> im2gz,
                           py::array_t<float> im2gy,
                           py::array_t<float> im2gx,
                           py::array_t<unsigned char> phases,
                           py::array_t<double> peaks,
                           py::array_t<double> M,
                           py::array_t<double> A);
void applyMeshTransformation(py::array_t<float> volGrey,       // image
                             py::array_t<unsigned int> volLab, // labels
                             py::array_t<float> volOut,        // return
                             py::array_t<unsigned int> conne,  // Connectivity Matrix      -- should be nTetrahedra * 4
                             py::array_t<double> nodes,        // Tetrahedra Points        -- should be nNodes      * 3
                             py::array_t<double> displ,
                             int numThreads);
void computeGradientPerTet(py::array_t<unsigned int> volLabel, // image
                            py::array_t<float> vol4DGrad,       // gradient
                            py::array_t<unsigned int> conne,    // Connectivity Matrix -- should be nTetrahedra * 4
                            py::array_t<double> nodes,          // Tetrahedra Points --   should be nNodes      * 3)
                            py::array_t<double> matOut);
void computeDICglobalMatrix(py::array_t<unsigned int> volLabel, // image
                            py::array_t<float> vol4DGrad,       // gradient
                            py::array_t<unsigned int> conne,    // Connectivity Matrix -- should be nTetrahedra * 4
                            py::array_t<double> nodes,          // Tetrahedra Points --   should be nNodes      * 3)
                            py::array_t<double> matOut);
void computeDICglobalVector(py::array_t<unsigned int> volLabelNumpy, // image
                            py::array_t<float> vol4DGradNumpy,
                            py::array_t<float> vol1Numpy,
                            py::array_t<float> vol2Numpy,
                            py::array_t<unsigned int> conneNumpy, // Connectivity Matrix -- should be nTetrahedra * 4
                            py::array_t<double> nodesNumpy,       // Tetrahedra Points --   should be nNodes      * 3)
                            py::array_t<double> vecOutNumpy);

void computeGMresidualAndPhase(py::array_t<float> im1,
                               py::array_t<float> im2,
                               py::array_t<unsigned char> phases,
                               py::array_t<double> peaks,
                               py::array_t<float> residual,
                               py::array_t<unsigned char> imLabelled);

void pixelSearch(py::array_t<float> im1,
                 py::array_t<float> im2,
                 py::array_t<float> argoutdata);
