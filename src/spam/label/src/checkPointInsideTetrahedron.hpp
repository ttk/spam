#include <stdio.h>
#include <math.h>
#include <iostream>
#include <Eigen/Dense>

char checkPointInsideTetrahedron( float Z, float Y, float X, Eigen::Matrix<float, 4, 3> pTetMatrix ); 

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
};
