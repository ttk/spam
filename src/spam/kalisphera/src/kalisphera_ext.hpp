/*  Copyright 2016 Edward Ando', Felix Bertoni, Alessandro Tengattini
                   
 *  This file is part of kalisphera_c.

    kalisphera_c is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    kalisphera_c is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with kalisphera_c.  If not, see <http://www.gnu.org/licenses/>
*/

/* FILE kalisphera_c.h BEGIN */

/// This def is for swiching easily between "float" and "double"
#define real_t double

///type representing a sphere by center and radius
typedef struct {
    real_t radius;
    real_t centerCoords[3];
} sphere_ts;

/// define used in Int_5 function... changing value can affect a little the precision
#define ZERO_PLUS_CONST 1e-10;

/* ########################################### */
/* ######## MAIN FUNCTIONS DECLARATION ####### */
/* ########################################### */
/* following functions are the main body of the code,
 * kalisphera_c is meant to be called by python script*/
/* ########################################### */

/** calculate the value of a voxel by dividing it in lot of subvoxels
 * 
 * 
 * 
 * 
 * 
 * */
real_t voxelValByForce(real_t voxelCoords[3], sphere_ts* sphere, int partCount, int isUpperValue);


/** return the (value partial volume effect) of the specified voxel
 * 
 *  Value of the voxel is about how much of the sphere volume is in
 *  the voxel (between 0.0 and 1.0)
 *  Note that this function uses inSphereRef and nextSphereRef,
 *  both are using a global var for the coordinate of the sphere center.
 *  
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param sphere NOT NULL the sphere to check
 * 
 *  @return [0.0;1.0] the value of the volume of the sphere contained by the voxel
 */
real_t voxelValue(real_t voxelCoords[3], sphere_ts* sphere);

/** return the value of the voxel in case of it is only partially in sphere
 *  
 *  this function uses all CubeCaseX functions defined below
 *  Note that this function uses inSphereRef and nextSphereRef,
 *  both are using a global var for the coordinate of the sphere center. 
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param sphere NOT NULL the sphere to check, 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param voxelCornersInNumber the number of corners in the sphere
 */
real_t voxelIntegralCase(real_t voxelCoords[3], sphere_ts* sphere, 
                         int voxelCorners[2][2][2], int voxelCornersInNumber);

/* ########################################## */
/* ######## MAIN FUNCTIONS DECL ENDS ######## */
/* ########################################## */

/* ########################################### */
/* ######## UTILS FUNCION DECLARATION ######## */
/* ########################################### */
/* Following function are made to calculate some useful
 * values and make the code clearer and coding faster */
/* ########################################### */

/** calculate distance between two points in 3D space
 *  
 *  distance is calculated between point 1(x1, y1, z1) and point 2(x2, y2, z2)
 *  
 *  @param z1 the depth coordinate of point 1
 *  @param y1 the height coordinate of point 1
 *  @param x1 the width coordinate of point 1
 *  @param z2 the depth coordinate of point 2
 *  @param y2 the height coordinate of point 2
 *  @param x2 the width coordinate of point 2
 * 
 *  @return the distance between point 1 and point 2
 */
real_t distance(real_t z1, real_t y1, real_t x1,
               real_t z2, real_t y2, real_t x2);

/** counts how much corners of one of the X surfaces of a voxel are in sphere
 * 
 *  This function is declined between X / Y and Z surfaces to gain speed
 *  this is the X (width) surface counter function
 *  
 *  @param xIndex the index of the width surface to count
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 */
int cornerCountSurfaceX(int xIndex, int voxelCorners[2][2][2]);


/** counts how much corners of one of the Y surfaces of a voxel are in sphere
 * 
 *  This function is declined between X / Y and Z surfaces to gain speed
 *  this is the Y (height) surface counter function
 *  
 *  @param xIndex the index of the height surface to count
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 */
int cornerCountSurfaceY(int yIndex, int Distances[2][2][2]);


/** counts how much corners of one of the Z surfaces of a voxel are in sphere
 * 
 *  This function is declined between X / Y and Z surfaces to gain speed
 *  this is the Z (depth) surface counter function
 *  
 *  @param xIndex the index of the depth surface to count
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 */
int cornerCountSurfaceZ(int zIndex, int voxelCorners[2][2][2]);

/** return if the voxel is in or not in the sphere 
 * 
 *  this function tests collision between voxel and sphere,
 *  and return the result 
 *  note that this function uses global variable for the sphere center coordinates
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param sphere NOT NULL the sphere to check for
 * 
 *  @return 1 if voxel is totally out of sphere
 *          -1 if voxel is totally in the sphere
 *          0 else
 */
int voxelInSphere(real_t voxelCoords[3], sphere_ts* sphere, real_t voxSize); 


/** change referal from grid origin to sphere center in the direction specified
 *  
 *  note that this function uses global variable for the sphere center coordinates
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param direction the direction to change referal 
 *                   0 = width
 *                   1 = heigth
 *                   2 = depth
 *  @param sphere NOT NULL the sphere to use for referal
 * 
 *  @return the coordinate of the voxel in specified direction,
 *          in the referal of the sphere center
 */
real_t inSphereRef(real_t voxelCoords[3], int direction, sphere_ts* sphere);

/** change referal from grid origin to sphere center in the direction specified for
 *  far corner of the voxel
 *  
 *  note that this function uses global variable for the sphere center coordinates
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param direction the direction to change referal 
 *                   0 = width
 *                   1 = heigth
 *                   2 = depth
 *  @param sphere NOT NULL the sphere to use for referal
 * 
 *  @return the coordinate of the voxel in specified direction,
 *          in the referal of the sphere center, +1.0
 */
real_t nextSphereRef(real_t voxelCoords[3], int direction, sphere_ts* sphere);

/* ########################################## */
/* ######## UTILS FUNCTION DECL ENDS ######## */
/* ########################################## */

/* ################################################## */
/* ######## INTEGRATION FUNCTION DECLARATION ######## */
/* ################################################## */
/* following function are used for calculating integrals,
 * wich are the values of voxels in case of the voxel is
 * have an intersection with the surface of the sphere */
/* ################################################### */


/* ## INTS ##*/
/* following funcs are used to calculate parts of
 * integralGroups. R is the radius of the sphere,
 * X and Y are values calculated from integralGroups */
real_t Int_1 (real_t X, real_t R);
real_t Int_2 (real_t X, real_t Y, real_t R);
real_t Int_3 (real_t X, real_t R);
real_t Int_4 (real_t X, real_t Y, real_t R);
real_t Int_5 (real_t X, real_t Y, real_t R);

/* INEGRALGROUPS */
/* This functions are grouping INTS funcs to calculate
 * the values of voxels.
 * R is the radius of sphere
 * Am the minimum of integration interval on direction A
 * AM the maximum of integration interval on direction A */
real_t integralGroup1(real_t Xm, real_t R);
real_t integralGroup2(real_t Xm, real_t XM, real_t Ym, real_t R);
real_t integralGroup3(real_t Xm, real_t XM, real_t Ym, real_t Zm, real_t R);
real_t integralGroup4(real_t Xm, real_t XM, real_t Ym, real_t YM, real_t Zm, real_t R);

/* ################################################ */
/* ######## INTEGRATION FUNCTION DECL ENDS ######## */
/* ################################################ */

/* ####################################### */
/* ######## CASE CUBE DECLARATION ######## */
/* ####################################### */
/* following functions calculate the value of a voxel,
 * depending its position and the position of the sphere */
/* ####################################### */

/** calculate value of a voxel wich have no corner in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube0 (real_t voxelCoords[3], sphere_ts* sphere);


/** calculate value of a voxel wich have 1 corner in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube1 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);

/** calculate value of a voxel wich have 2 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube2 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);


/** calculate value of a voxel wich have 3 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube3 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);


/** calculate value of a voxel wich have 4 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube4 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);


/** calculate value of a voxel wich have 5 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube5 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);


/** calculate value of a voxel wich have 6 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case 
 */
real_t caseCube6 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);


/** calculate value of a voxel wich have 7 corners in the sphere
 *  
 *  please notice that this function is using global variable for the center of the sphere
 * 
 *  @param voxelCoords an array of the three coodinates of the voxel, 
 *                     in order{width, height, depth} 
 *  @param voxelCorners a 2x2x2 matrix of (int) boolean, representing
 *                      wich corners of the voxel are in the sphere or not
 *  @param sphere NOT NULL the sphere to use for referal
 *
 *  @return the value of the voxel throught this case
 */
real_t caseCube7 (real_t voxelCoords[3], int voxelCorners[2][2][2], sphere_ts* sphere);

/* ##################################### */
/* ######## CASE CUBE DECL ENDS ######## */
/* ##################################### */

/* FILE kalisphera_c.h ENDS */
