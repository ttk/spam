.. spam documentation master file, created by
   sphinx-quickstart on Tue Sep 12 22:24:53 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to spam's documentation!
========================================

.. toctree::
    :maxdepth: 2
    :caption: Getting started

    intro.rst
    installation.rst
    gettingStarted.rst
    conventions.rst
    howToContribute.rst

.. toctree::
    :maxdepth: 3
    :caption: Scripts

    scripts



.. toctree::
    :maxdepth: 2
    :caption: Tutorials

    tutorials/index



.. toctree::
    :maxdepth: 2
    :caption: Examples

    spam_examples/index



.. toctree::
    :maxdepth: 1
    :caption: Some notes

    overlapping
    vtk




.. toctree::
   :maxdepth: 1
   :caption: Reference documentation

   indices/index
