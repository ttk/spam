﻿###############
Getting started
###############

In order to use spam you will always have to **activate its virtual environment** where it is installed:

.. code-block:: console

    $ source /path/to/spam/venv/bin/activate
    (spam) $

1. Using spam in your scripts
-----------------------------

You can **include spam in your own scripts** by importing different modules (see `module index`_)

.. code-block:: python

    import spam.DIC

A lot of spam functionalities are available as atomic functions.
Please refer to the :ref:`sphx_glr_spam_examples` for use of spam functions in python.

2. Using existing spam scripts
------------------------------

We also provide more sophisticated functionality in **spam-scripts**, which are called directly from the command line (see :ref:`scriptsTutorial`).

.. _module index: https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/py-modindex.html

