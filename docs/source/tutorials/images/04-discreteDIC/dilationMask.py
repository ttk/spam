
import tifffile
import spam.labelled
import matplotlib
font = {'family' : 'CMU Bright',
        'weight' : 'normal',
        'size'   : 16}
matplotlib.rc('font', **font)

lab = tifffile.imread("LENGP04_00-lab.tif")
grey = tifffile.imread("LENGP04_00.tif")
myLabelNumber = int(lab.max()//2)

myLabelD0 = spam.label.getLabel(lab, myLabelNumber, extractCube=True, extractCubeSize=25)
myLabelD2 = spam.label.getLabel(lab, myLabelNumber, extractCube=True, extractCubeSize=25, labelDilate=2)
myLabelD4 = spam.label.getLabel(lab, myLabelNumber, extractCube=True, extractCubeSize=25, labelDilate=4)

plt.subplot(2,4,1)
plt.title('Raw data')
plt.imshow(lab[myLabelD0['sliceCube']][25], cmap=spam.label.randomCmap)
plt.subplot(2,4,2)
plt.title('No dilation')
plt.imshow(myLabelD0['subvol'][25], cmap=spam.label.randomCmap)
plt.subplot(2,4,3)
plt.title('Dilation = 2px')
plt.imshow(myLabelD2['subvol'][25], cmap=spam.label.randomCmap)
plt.subplot(2,4,4)
plt.title('Dilation = 4px')
plt.imshow(myLabelD4['subvol'][25], cmap=spam.label.randomCmap)
plt.subplot(2,4,5)
plt.imshow(grey[myLabelD0['sliceCube']][25], cmap='Greys_r')
plt.subplot(2,4,6)
plt.imshow((grey[myLabelD0['sliceCube']]*myLabelD0['subvol'])[25], cmap='Greys_r')
plt.subplot(2,4,7)
plt.imshow((grey[myLabelD0['sliceCube']]*myLabelD2['subvol'])[25], cmap='Greys_r')
plt.subplot(2,4,8)
plt.imshow((grey[myLabelD0['sliceCube']]*myLabelD4['subvol'])[25], cmap='Greys_r')
plt.show()
