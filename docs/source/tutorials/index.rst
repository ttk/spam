.. _tut:

Gallery of Tutorials
======================

Set of tutorials for a smooth introduction to spam

.. toctree::
    :maxdepth: 1

    tutorial-00-refreshments
    tutorial-02a-DIC-theory.rst
    tutorial-02b-DIC-practice.rst
    tutorial-07-strain
    tutorial-03-labelToolkit
    tutorial-08-contacts
    tutorial-04-discreteDIC
    tutorial-06-projection
    tutorial-09-multimodal_registration

