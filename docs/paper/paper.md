---
title: '`spam`: Software for Practical Analysis of Materials'
tags:
  - Python
  - C/C++
  - material science
  - mechanics of materials
  - granular mechanics
  - digital image correlation
  - digital volume correlation
  - mesh projection
  - morphology
authors:
  - name: Olga Stamati
    orcid: 0000-0002-7492-2134
    affiliation: 1
  - name: Edward Andò
    orcid: 0000-0001-5509-5287
    affiliation: 1
  - name: Emmanuel Roubin
    orcid: 0000-0002-5748-139X
    affiliation: 1
  - name: Rémi Cailletaud
    orcid: 0000-0002-1373-7031
    affiliation: "1, 2"
  - name: Max Wiebicke
    orcid: 0000-0001-9556-8579
    affiliation: "1, 3"
  - name: Gustavo Pinzon
    orcid: 0000-0002-6526-7393
    affiliation: 1
  - name: Cyrille Couture
    orcid: 0000-0001-9073-3926
    affiliation: 1
  - name: Ryan C. Hurley
    orcid: 0000-0003-1652-635X
    affiliation: 4
  - name: Robert Caulk
    orcid: 0000-0001-5618-8629
    affiliation: 1
  - name: Denis Caillerie
    affiliation: 1
  - name: Takashi Matsushima
    orcid: 0000-0002-2553-7643
    affiliation: 5
  - name: Pierre Bésuelle
    orcid: 0000-0001-9586-4888
    affiliation: 1
  - name: Félix Bertoni
    affiliation: 6
  - name: Tom Arnaud
    affiliation: 6
  - name: Alejandro Ortega Laborin
    orcid: 0000-0001-9026-5288
    affiliation: 1
  - name: Riccardo Rorato
    affiliation: 7
  - name: Yue Sun
    orcid: 0000-0002-9154-282X
    affiliation: 8
  - name: Alessandro Tengattini
    orcid: 0000-0003-0320-3340
    affiliation: "1, 9"
  - name: Olumide Okubadejo
    affiliation: 1
  - name: Jean-Baptiste Colliat
    affiliation: 8
  - name: Mohammad Saadatfar
    affiliation: 10
  - name: Fernando E. Garcia
    orcid: 0000-0001-7993-0347
    affiliation: 11
  - name: Christos Papazoglou
    affiliation: 1
  - name: Ilija Vego
    orcid: 0000-0003-4426-3382
    affiliation: 1
  - name: Sébastien Brisard
    orcid: 0000-0002-1976-6263
    affiliation: 12
  - name: Jelke Dijkstra
    orcid: 0000-0003-3792-0727
    affiliation: 13
  - name: Georgios Birmpilis
    orcid: 0000-0002-8353-693X
    affiliation: 13

<!-- affiliation: "1, 2" # (Multiple affiliations must be quoted) -->
affiliations:
 - name: Univ. Grenoble Alpes, CNRS, Grenoble INP, 3SR, F-38000 Grenoble, France
   index: 1
 - name:  Univ. Grenoble Alpes, CNRS, INRAE, IRD, Météo France, OSUG, 38000 Grenoble, France
   index: 2
 - name: Institute of Geotechnical Engineering, Technische Universität Dresden, D-01062 Dresden, Germany
   index: 3
 - name: Johns Hopkins University, Baltimore, MD 21218, USA
   index: 4
 - name: Department of Engineering Mechanics and Energy, University of Tsukuba, Tsukuba, Japan
   index: 5
 - name: Univ. Grenoble Alpes, IUT2, 38000 Grenoble, France
   index: 6
 - name: Universitat Politécnica de Catalunya (UPC), Barcelona (Spain) - Department of Civil and Environmental Engineering
   index: 7
 - name: Univ. Lille, CNRS, Centrale Lille, UMR 9013 - LaMcube - Laboratoire de Mécanique, Multiphysique, Multiéchelle, F-59000 Lille, France
   index: 8
 - name: Institut Laue-Langevin (ILL), Grenoble, France
   index: 9
 - name: Australian National University, Canberra, Australia
   index: 10
 - name: Division of Engineering \& Applied Science, California Institute of Technology, Pasadena, CA 91125, USA
   index: 11
 - name: Université Gustave Eiffel, Laboratoire Navier, ENPC, IFSTTAR, CNRS UMR 8205, Marne-la-Vallée, F-77455, France
   index: 12
 - name: Department of Architecture and Civil Engineering, Chalmers University of Technology, SE-412 96 Gothenburg, Sweden
   index: 13
date: March 2020
bibliography: paper.bib

# Optional fields if submitting to a AAS journal too, see this blog post:
# https://blog.joss.theoj.org/2018/12/a-new-collaboration-with-aas-publishing
<!-- aas-doi: 10.3847/xxxxx <- update this with the DOI from AAS once you know it. -->
<!-- aas-journal: Astrophysical Journal <- The name of the AAS journal. -->
---


# Statement of need

Advanced experimental materials science is entering a new era thanks to the performance and availability of tomography allowing full-3D studies, and often 3D-timeseries.
However in the analysis of these advanced measurements, the attractiveness of closed-source and black-box solutions to 3D measurement problems seems to be precipitating a reproducibility crisis in this domain.
A radically open approach to measurement science with the disclosure of analysis tools will allow validation and verification of tools and results, rendering the whole chain more robust.
Furthermore, the hope is that there is a community-driven improvement of the tools with time.


# Summary

Spam, the Software for the Practical Analysis of Materials is a Python library that has evolved to cover needs of data analysis from 3D x-ray tomography work and correlated random fields with mechanical applications.
Spam is first and foremost a measurement package that extends the extremely convenient framework of NumPy [@numpy] and SciPy [@SciPy2020] by providing or accelerating tools for the material- science/mechanics oriented analysis of 2D images or 3D volumes representing field measurements.
Typical uses are either the measurement of displacements fields between images of a deforming sample from which strains can be computed, or the characterisation of a particular microstructure (correlation length or particle orientation).
The package is organised into a library of Python tools which are expected to be used in user-written scripts and a number of more sophisticated standalone scripts.
The tools are organised as follows:

- `DIC`: toolkit for Digital Image/Volume Correlation. This toolkit provides tools to measure the deformation between two images/volumes. A robust registration tool based on the same-modality version of @tudisco2017extension is provided, as well as some cutting edge tools such as multi-modal registration that implements @roubin2019colours.
A version of Global-DVC as per @mendoza2019complete is in the process of being developed.
The `DIC` toolkit is presented in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html), discussed in detail in tutorials covering [theory](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-02a-DIC-theory.html), [practice](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-02b-DIC-practice.html) and [discrete DIC](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-04-discreteDIC.html), and illustrated in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)


- `deformation`: toolkit for manipulating deformation functions called $\Phi$ (expressed in homogeneous coordinates), as well as the more usual in continuum mechanics transformation gradient tensor $F$.
Tools are also provided to compute fields of $F$ from a displacement field measured on a regular grid (coming from a "local" correlation for example), either using square/cubic finite element shape functions, or the method proposed in @geers1996computing.
For a displacement field on an irregular grid (for example defined at particle centres coming from a "discrete" correlation), a Delaunay triangulation based method [@bagi1996stress; @zhang2015large; @catalano2014] is implemented.
Both finite (large) strain and infinitesimal (small) strain frameworks are implemented for both regular and irregular grids.
The `deformation` toolkit is presented (together with `DIC`) in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html), discussed in detail in the [continuum mechanics refresher](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-00-refreshments.html), the start of [DIC theory](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-02a-DIC-theory.html) and [strain tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-07-strain.html), and is used throughout the DIC examples (see above)

- `excursions`: toolkit for the excursion set of correlated random fields theory [@adler2008new].
It includes functions that give the analytical predictions of the global descriptors (or Lipschitz-Killing curvatures) of excursions in spaces of arbitrary dimensions [@roubin2015meso; @roubin2016perco] along with the generation of correlated random fields using `RandomFields` in R through `rpy2` [@Ritself; @rpy2; @schlather2015randomfields].
The `excursions` toolkit is illustrated in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)


- `filters`: toolkit of 3D filters that provide some functionality missing in `scipy.ndimage.filters` such as the computation of a local hessian, or functions which are slow.
For example, for the computation of a local variance, the spam-provided function is more than 100 times faster than using the generic filter with variance in `scipy.ndimage`.
The `filters` toolkit is used in the [projection tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-06-projection.html), and illustrated [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)

- `helpers`: toolkit of internal helper functions such as the parsers for the scripts, as well as tools for reading and writing TSV and VTK files.
The latter partially uses `meshio` [@meshio]

- `kalisphera`: wrapper for C++ version of `kalisphera` [@tengattini2015kalisphera] which generates analytically-correct partial-volume spheres which are useful for testing discrete analysis code (see `label` below).
The `kalisphera` toolkit is presented in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html), is used in the [contacts tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-08-contacts.html), and is illustrated in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)

- `label`: toolkit to measure and manipulate "labelled" images, where discrete particles are labelled with integer voxel patches.
The computation of standard quantities such as the volume, centre of mass, and moment of inertia tensor of each particle can be done very conveniently and somewhat faster and with a smaller memory footprint than what is available in `scipy.ndimage`.
An estimation of an ellipse fitting of each particle is implemented with the algorithm from @ikeda:2000.
Tools for characterising inter-particle contacts based on the work of @wiebicke2017.
A wrapper for ITK's morphological watershed [@schroeder2003itk; @beare2006watershed] is also provided.
The `label` toolkit is presented in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html), discussed in detail in tutorials covering [the base toolkit](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-03-labelToolkit.html) and [contacts](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-08-contacts.html), and illustrated in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)

- `measurements`: toolkit implementing the measurement of covariance, porosity and global descriptors (volume, perimeter, surface area, and Euler characteristic).
The `measurements` toolkit is used in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)

- `mesh`: toolkit for the creation or manipulation of meshes -- in `spam` tetrahedral meshes are principally used. Meshers based on Gmsh [@geuzaine2009gmsh] are used through `pygmsh` and weighted Delaunay triangulation (Laguerre triangulation) is provided through an interface with CGAL [@cgal] -- alpha-shapes are also implemented to help clean up badly-shaped tetrahedra.
In addition, a set of projection functions creates meshes able to represent heterogeneities (phases and interfaces of a given meso/micro structure) based on binary or trinary images or continuous fields (level set) with outputs easily convertible to any FE software [@roubin2015multi; @stamati2018tensile].
The `mesh` toolkit is presented in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html), at the heart of the [projection tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-06-projection.html), and illustrated in the [examples gallery](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/spam_examples/index.html)

- `plotting`: toolkit of plotting tools based on `matplotlib` [@matplotlib] for creating complex plots such as a 3D orientation plot.
The `plotting` toolkit is presented in the [introduction](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/intro.html) and used throughout tutorials and examples

- `visual`: toolkit of graphical helper functions for scripts


A number of scripts are available to be called from the command line.
Currently, the most-used scripts are related to image correlation:

- `spam-ldic` and `spam-regularStrain`: A "local" image correlation script, working for series of greyscale 2D or 3D images where kinematics are measured on independent points spread on a regular grid, accompanied by a strain computation script.
`spam-ldic` is presented in the [scripts page](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/scripts.html) and is the subject of the [DIC practice](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-02b-DIC-practice.html).
`spam-regularStrain` is presented in the [scripts page](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/scripts.html) and is used in the [strain tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-07-strain.html)

- `spam-ddic` and `spam-discreteStrain`: A "discrete" image correlation script [@hall2010discrete; @ando2012experimental], working on greyscale 3D images plus a "labelled" image of the reference configuration. This script also has its own strain calculation based on a triangulation of grain centres.
`spam-ddic` is presented in the [scripts page](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/scripts.html) and is the subject of the [discrete DIC tutorial](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/tutorial-04-discreteDIC.html)

- `spam-gdic` (in beta test): A "global" image correlation script, where the displacement field between two 3D images is computed as a global problem expressed on a tetrahedral mesh

- `spam-mmr` and `spam-mmr-graphical`: A pair of "multi-modal registration" scripts (command-line and graphical) allowing 3D images of different modalities (*e.g.*, a neutron tomography and an x-ray tomography of the same sample) to be registered (*i.e.*, aligned)


# Technical details

Spam is based on simple Python data types, avoiding complex data structures, and all functions have a reasonable and safe set of default parameters, with required parameters kept to a minimum.
Spam has a number of different use cases:

- Interactive use, such as in iPython or Jupyter. Many outputs from 3D analysis in materials science are highly sensitive to the parameters used, encouraging a "live" exploration of optimal settings

- Embedded use, such as importing in user-written Python scripts

- Standalone use, particularly of the more complex `spam-` scripts. These chain together a number of functions and are intended to be called from a command line, and produce output as live plots, or files saved to disk

Given the large data volumes often encountered in 3D analysis, critical parts of the code are written in C/C++ wrapped with appropriate Python calling functions which are responsible for checking input sanity.
The current wrapping method is with pybind11 [@pybind11].

Building on the large number of functions already available in NumPy [@numpy], SciPy [@SciPy2020], scikit-image [@scikitimage] and making use of tifffile [@tifffile] and meshio [@meshio], the spam project adds a large amount of functionality, which means that a number of advanced forms of data analysis can be chained together in ways that are otherwise complex, requiring the combination of many different tools.

Spam uses `unittest` to check each commit with a coverage of more than 90\% of lines of code covered as of June 2020.
[Details of the coverage](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/coverage/) are available on the GitLab repository.


# Online documentation

The documentation for this project is available online at this address:

  [https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/index.html](https://ttk.gricad-pages.univ-grenoble-alpes.fr/spam/index.html)

There are three main components to the documentation:

- The module index is built automatically by `sphinx`, and function headers are written in such a way (following the NumPy norm) that a brief description appears in the module index

- A "Gallery of Examples" using `sphinx-gallery` where downloadable Python scripts or Jupyter notebooks are executed during the compilation of the documentation and whose results are visible

- A series of "Tutorials" with longer and more detailed explanations are available, which cover the mathematical/mechanics background of the functions provided, and some longer examples of using the provided tools


<!-- # Other software available -->
# State of the field

Other software packages exist for material science analysis, such as the popular commercial software Avizo, which is closed-source, cannot be inspected, and therefore has limited trust.
Other open-source packages include ITK [@schroeder2003itk], which is a quite complex ecosystem, and ImageJ/Fiji [@imagej2], which is not fully 3D and not well-suited to scripting or running remotely.
All of the above have some version of the `label` toolkit which allows discrete objects to be characterised as well as some parts of the `measurements` toolkit.

On the specific topic of Digital Image/Volume Correlation, many pieces of software are available for 2D, surface and 3D image correlation.
The `spam.DIC.register` correlation engine uses a well-known mathematical framework dating back to [@lucasKanade], but distinguishes itself by being non-rigid, 2/3D compatible, clearly documented and relatively fast.
The discrete and multimodal image correlation are much less common.
A number of other image correlation codes exist (this is really not an exhaustive list):

- CMV and CMV\_3D:   Developed at Laboratoire Navier [@bornert2004mesure], local and non-rigid code allowing discrete DVC used in @hall2010discrete

- Correlli:   Developed by LMT Cachan, shared with colleagues but not open source [@hild2008correliq4]. Contains a cutting edge integrated DVC global approach

- FIDVC and qDIC:     Open source code 2D running on Matlab [@bar2014fast; @Landauer2018] from The Franck Lab which is suitable for measuring large transformations

- TomoWarp2: Developed by some of the co-authors [@tudisco2017tomowarp2]. This software has a graphical interface for facilitating correlation but is technically limited to displacements/rotations, and has a slow line-search in rotation space

- muDIC:     Pure Python toolkit, limited to 2D images [@muDIC] but has a fully function global approach

- UFreckles: Graphical Matlab code [@ufreckles] working on 2D, surface and 3D images with a global approach

- StrainMaster: Closed-source software for surface and 3D image correlation from LaVision

- VIC3D:     High-speed closed-source software for surface imaging from Correlated Solutions


# Getting `spam`

`spam` is available for Linux users with Python 3 as a PyPI package installed via `pip`.
Developers or curious users are encouraged to clone the [Git repository](https://gricad-gitlab.univ-grenoble-alpes.fr/ttk/spam).
Solid build instructions exist for Debian-based environments (in principle any GNU/Linux distribution should work) and for some versions of OSX.
Compilation for Windows has not been attempted given the large number of dependencies. 
However, users have been able to get `spam` to work using Ubuntu in the Windows Subsystem for Linux (WSL).

# Use in existing work

Spam has already enabled research progress on a number of fronts, resulting in the following publications:

- @roubin2016perco: Use of `excursions` toolkit to predict percolation threshold in n-dimensional Euclidean spaces (Figures 1 and 2)

- @stamati2018phase: Use of `filters` toolkit to identify aggregates in concrete (Figure 5)

- @stamati2018tensile: Use of `spam-ldic` and `spam-ddic` scripts to measure deformation in a concrete sample subjected to a tension test (Figure 8) and `mesh` projection functions to conduct the FE analysis (Figure 2)

- @stavropoulou2019liquid: Use of `spam-ldic` script to measure deformation of a claystone (Figures 10 and 11)

- @wiebicke2019benchmark: Use of `kalisphera` (Figure 5) and `label` toolkits to benchmark sand-grain contact measurements (Figure 8 and others), provides an example script

- @ando2019peek: Use of `spam-ddic` script and the `label` toolkit to measure small displacements in a creep test on sand, see Figure 4

- @roubin2019colours: Application of Multi Modal Registration to concrete (Figure 4 onwards)

- @hurley2019situ: Use of `deformation` toolkit to measure deformation in concrete (strain in Figure 2b)

- @wiebicke2020measuring: Use of `label` toolkit for the analysis of inter-particle contacts (Figure 1) as well as the `plotting` toolkit to plot the distribution of orientations (Figures 7 and 8)

- @stavropoulou2020: Use of `spam-mmr` and `spam-gdic` scripts (Figures 2, 3, 4 and Figure 7 respectively), and the `mesh` toolkit to measure water absorption in claystone (Figures 8 and 9)

# Acknowledgements

We would like to acknowledge:

- Cino Viggiani, Jacques Desrues, and Bruno Chareyre in Laboratoire 3SR, Grenoble
- Stéphane Roux and François Hild in LMT Cachan
- Emmanuelle Gouillard in Saint Gobain/CNRS and Plotly
- Jose Luis Cercos-Pita at the ESRF and Uppsala University

for discussions that have contributed to getting spam into its current form.

# References
