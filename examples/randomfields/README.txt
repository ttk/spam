.. _randomfields_examples:

Random fields and excursion sets
--------------------------------

This is a set of examples for generating correlated random fields working with their excursions.
