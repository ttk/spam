# -*- coding: utf-8 -*-
"""
Simulation of correlated Random Fields
======================================

Example of a generation of 2D correlated Random Field
"""

import spam.excursions
import matplotlib.pyplot as plt

# define the covariance
covariance = {'type': 'stable', 'alpha': 2.0, 'variance': 1.0, 'correlation_length': 0.1}

# generate one realisation
realisation = spam.excursions.simulateRandomField(nNodes=100, covariance=covariance, dim=2)

# plot
plt.imshow(realisation)
plt.title("Realisation")
plt.show()
